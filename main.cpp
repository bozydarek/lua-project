#include <iostream>
#include <sys/stat.h>
#include <memory>

#include "src/usersdatabase.h"
#include "src/usersdatabaselocatedinfile.h"

#include "src/LuaState.h"
#include "src/serverConfiguration.h"
#include "src/Server.h"
#include "src/ThreadFunctions.h"

using namespace std;

void manual()
{
    cout << "\033[1;36mLuaGameServer\033[0m - universal server to host programming games\n";
    cout << "\n\033[1mUsage:\033[0m\n";
    cout << "\t./LuaGameServer -c CONF_FILE         - to start server with configuration specified in Lua file\n";
    cout << "\t./LuaGameServer -p PORT -g GAME_FILE - to start server on PORT with game from GAME_FILE\n";
//    cout << "\t./LuaGameServer -p PORT -d GAME_DIR  - to start server on PORT with all games form GAME_DIR\n";
    cout << "\t./LuaGameServer -h                   - for display this help\n";
    cout << "\n";
    cout << "CONF_FILE - Lua file with configuration to server. For more look to documentation\n";
    cout << "PORT      - Number of port for server to start listen\n";
    cout << "GAME_FILE - Lua file with configuration for one game. For more look to documentation\n";
//    cout << "GAME_DIR  - Directory with games files\n";
    cout << "\n\033[1mFor example:\033[0m\n";
    cout << "\t./LuaGameServer -c conf.lua\n";
    cout << "\t./LuaGameServer -p 4444 -d ./games\n";
}

void parsingParametersError(string reason) {
    cout << "\033[31;1mError:\033[0m " << reason << endl;
    exit(-1);
}

void checkNextParameter(string flag, int i, int arg_count)
{
    if( i+1 >= arg_count ){
        parsingParametersError("You have to pass argument after -"+flag+"\nFor more information try -h flag");
    }
}

string resolvePath(char *path) {
    char resolved_path[PATH_MAX];
    if ( realpath(path, resolved_path) == NULL )
    {
        parsingParametersError(" Can't resolve given path: "+string(path));
    }

    return string(resolved_path);
}

//bool is_directory(string path){
//
//    struct stat info;
//
//    if( stat( path.c_str(), &info ) != 0 )
//    {
//        return false;
//    }
//
//    return (info.st_mode & S_IFDIR) != 0;
//
//}

bool is_file(string path){

    struct stat info;

    if( stat( path.c_str(), &info ) != 0 )
    {
        return false;
    }

    return (info.st_mode & S_IFREG) != 0;

}

int main(int arg_count, char *arg_value[])
{
    auto &serverConfig = serverConfiguration::getConfiguration();

    for (int i = 1; i < arg_count; ++i) {
        string argument = arg_value[i];
//        unsigned long arg_length = argument.length();

        if(argument == "-h"){
            manual();
            return 0;
        }
        else if(argument == "-c")
        {
            checkNextParameter("c",i,arg_count);

            string configFileName = arg_value[i+1];

            switch ( serverConfig.loadConfiguration(configFileName) ){
                case 1:
                    parsingParametersError("You have to pass config file in LUA after -c\n"
                                               "File " + configFileName + " doesn't exist or isn't a lua file");
                    break;

                case 2:
                    parsingParametersError("File " + configFileName + " hasn't got proper structure");
                    break;

                case 3:
                    parsingParametersError("Some games files from " + configFileName + " config file "
                                           "don't load properly");

                case 0:
                    cout << "Load server configuration properly" << endl;
                    break;

                default:
                    parsingParametersError("Unknown response from serverConfig load");
            }

            ++i;
        }
        else if(argument == "-p")
        {
            checkNextParameter("p",i,arg_count);

            int port = atoi(arg_value[i+1]);

            if(port < 1){
                parsingParametersError("You have to chose port grater then 0\n");
            }

            serverConfig.setPort(port);

            ++i;
        }
        else if(argument == "-g")
        {
            checkNextParameter("g",i,arg_count);

            string game_file = resolvePath(arg_value[i+1]);

            if( not is_file(game_file) ){
                parsingParametersError("File: "+ game_file + " doesn't exist or isn't a file\n");
            }

            if(not serverConfig.addGameToList(game_file)){
                parsingParametersError("File: "+ game_file + " is not proper luaGame. Check documentation for help.\n");
            }

            ++i;
        }
//        else if(argument == "-d")
//        {
//            checkNextParameter("d",i,arg_count);
//
//            string games_dir = resolvePath(arg_value[i+1]);
//
//            if( not is_directory(games_dir) ){
//                parsingParametersError("Directory: "+ games_dir + " doesn't exist or isn't a directory\n");
//            }
//
//            serverConfig.setGame_dir(games_dir);
//
//            ++i;
//        }
        else{
            parsingParametersError("Unknown argument: "+ argument +"\n");
        }

    }

    serverConfig.printConfiguration();
    cout << "\033[1mStart server\033[0m on port " << serverConfig.getPort() << endl;

    std::shared_ptr<UsersDatabase> database = std::make_shared<UsersDatabaseLocatedInFile>(serverConfig.getDatabase_file());

    if(database->getNumberOfUsers() == 0)
    {
        cout << "\033[35;1mWarning\033[0m there are no users in database!" << endl;
    }

    std::shared_ptr<ThreadSafeQueue<string>> loggerQueue = std::make_shared<ThreadSafeQueue<string>>();

    bool serverShouldEnd = false;
    std::thread loggerThread (loggerThreadFunction, loggerQueue, std::ref(serverShouldEnd));

    Server server(database,
                  (uint16_t) serverConfig.getPort(),
                  serverConfig.getFullPathToGamesFiles(),
                  loggerQueue,
                  serverShouldEnd);
    server.loop();

    loggerThread.join();

    return 0;
}

