score = 100
bonus = 2.5
scoreMultByBonus = score * bonus
hide = true;
local intVar = 42;

gameName = "Bad Game!"
shortGameName = "BAD"
minimumNumberOfPlayers = 1
maximumNumberOfPlayers = 10

function prepareGame(listOfPlayersIDs) -- VOID
    print("Players no.:",#listOfPlayersIDs)
    for i, v in ipairs(listOfPlayersIDs) do
        print("*", i, v)
    end
end

function startGame() -- VOID - additional function to handle everything that have to be handle after prepareGame but before first round

end

function handlePlayerMove(playerID, move) -- STRING - return results in one string that would be strict passed to user
    return "OK"
end

function playRound() -- VOID - function to play play round and prepare to next

end

function getScore() -- MAP <INT,INT> - function to get score, should contains playerID and score
    return "HAHAHA"
end

function notify() -- MAP <INT,STRING> - function that would be call after playRound, should contains playerID and info per player
    return {wat=23, [1]=1}
end

function isGameOver(costam) -- BOOL - function that return true if game is over and server should restart it
	costam = costam + 1
    return false
end

-- TODO: propably we need to add some functions for admin and observers
function getView() -- STRING - function that return all data needed by observer for example to write map or game board
    return "No view available"
end

function handleAdminFunctions(message)  -- STRING - function only for admins to handle additional commands (if needed)
    return "No admin functions available"
end