gameName = "Maze"
shortGameName = "MAZE"
minimumNumberOfPlayers = 1
maximumNumberOfPlayers = 100

local roundNumber = 1
local maxRoundNumber = 1
local scanRange = 2 -- radius

math.randomseed( os.time() )
local maze = {}
local players = {}

function prepareGame(listOfPlayersIDs)
    for _, v in ipairs(listOfPlayersIDs) do
        players[#players+1] = {pos={x=0,y=0}, newpos={x=0,y=0}, name=v, score=0, canMove=false, waiting=false}
    end
end

function startGame()
    local w = math.random( 16, 64 )
    local h = math.random( 8, 32 )
    maze = make_maze(w,h)

    maxRoundNumber = 2*(w)*(h)
    scanRange = math.floor( (math.log(w*h)/math.log(10))+0.5 )

    for _,p in ipairs(players) do
        copyPos(p["pos"], maze["start"])
        copyPos(p["newpos"], maze["start"])
        p["canMove"] = true
    end
end

function copyPos(dest, source)
    dest["x"] = source["x"]
    dest["y"] = source["y"]
end

function IsPosEq(dest, source)
    return dest["x"] == source["x"] and dest["y"] == source["y"]
end

-- function getXY(move)
--     local res = {}
--     for i in string.gmatch(move, "%S+") do
--         res[#res+1] = i
--     end

--     if res[2] == "N" then
--         return 1, 0
--     elseif res[2] == "S" then
--         return -1, 0
--     elseif res[2] == "E" then
--         return 0, 1
--     elseif res[2] == "W" then
--         return 0, -1
--     end
        
--     return nil
-- end

function getXY(move)
    local res = {}
    for i in string.gmatch(move, "%S+") do
        res[#res+1] = i
    end

    return tonumber(res[2]), tonumber(res[3])
end

function handlePlayerMove(playerID, move)
    for _, v in ipairs(players) do
        if playerID == v["name"] then
            if move:sub(1,4) == "MOVE" then
                if v["canMove"] then
                    local x,y = getXY(move)
                    if type(x) == "number" and type(y) == "number" and x*y == 0 and math.abs(x) <= 1 and math.abs(y) <= 1 then
                        v["canMove"] = false;
                        v["newpos"]["x"] = v["pos"]["x"] + x
                        v["newpos"]["y"] = v["pos"]["y"] + y
                        
                        local field = maze["data"][v["newpos"]["y"]][v["newpos"]["x"]]

                        if field==false or field=="S" or field=="F" then
                            return "OK"
                        else
                            copyPos(v["newpos"],v["pos"])

                            return "FAIL"
                        end
                    end
                    return "Wrong arguments to move"
                else
                    return "You can't move now."
                end
            elseif move == "SCAN" then
                if v["canMove"] then
                    v["canMove"] = false;
                    return scan(v["pos"])
                else
                    return "FAIL"
                end
            elseif move == "INFO" then
                return getInfo()
            elseif move == "ROUNDS LEFT" then
                return tostring(maxRoundNumber-roundNumber)
            elseif move == "WAIT" then
                v["waiting"] = true
                return "OK"
            end

            return "UNKNOWN COMMAND"
        end
    end
    return "UNKNOWN PLAYER"
end

function scan(pos)
    local map = maze["data"]
    local res=""
    local n=0
    for x=-scanRange, scanRange do
        for y=-scanRange, scanRange do
            local xx = pos["x"] + x
            local yy = pos["y"] + y
            if xx > 0 and yy > 0 and map[yy] and map[yy][xx] then
                if map[yy][xx] == "S" or map[yy][xx] == "F" then
                    res = res .. map[yy][xx] .. " " .. tostring(x) .. " " .. tostring(y) .. "\n"
                else
                    res = res .. "W " .. tostring(x) .. " " .. tostring(y) .. "\n"
                end
                n = n + 1
            end
        end
    end
    return tostring(n).."\n"..res
end

function getInfo()
    local res="ROUND "..tostring(roundNumber).."\n"
            .."MAXROUND "..tostring(maxRoundNumber).."\n"
            .."WIDTH "..tostring(maze["width"]).."\n"
            .."HEIGHT "..tostring(maze["height"]).."\n"
            .."SCAN_R "..tostring(scanRange).."\n"
            -- .."PLAYERS "..tostring(#players).."\n"
    return res
end

function playRound()
    roundNumber = roundNumber + 1

    for _, v in ipairs(players) do
        copyPos(v["pos"], v["newpos"])

        if IsPosEq(v["pos"], maze["finish"]) then
            v["score"] = maxRoundNumber - roundNumber
        else
            v["canMove"] = true
        end
    end
    -- print(maze2string(maze))
end

function getScore()
    local res = {}
    for _, v in ipairs(players) do
        res[v["name"]] = v["score"]
    end
    return res
end

function notify()
    local res = {}
    for _, v in ipairs(players) do
        if v["waiting"] then
            res[v["name"]] = "OK"
            v["waiting"] = false
        end
    end
    return res
end

function isGameOver()
    return (roundNumber > maxRoundNumber)
end

function getView()
    return maze2string(maze)
end

function handleAdminFunctions(message)
    return "No admin functions available"
end

-- Fisher-Yates shuffle from http://santos.nfshost.com/shuffling.html
function shuffle(t)
  for i = 1, #t - 1 do
    local r = math.random(i, #t)
    t[i], t[r] = t[r], t[i]
  end
end
 
-- builds a width-by-height grid of trues
function initialize_grid(w, h)
  local a = {}
  for i = 1, h do
    table.insert(a, {})
    for j = 1, w do
      table.insert(a[i], true)
    end
  end
  return a
end
 
-- average of a and b
function avg(a, b)
  return (a + b) / 2
end
 
 
dirs = {
  {x = 0, y = -2}, -- north
  {x = 2, y = 0}, -- east
  {x = -2, y = 0}, -- west
  {x = 0, y = 2}, -- south
}
 
function make_maze(w, h)
  local map = initialize_grid(w*2+1, h*2+1)
  
  local allDeads = {}
  local len = 0

  local function walk(x, y)
    map[y][x] = false
 
    local d = { 1, 2, 3, 4 }
    local deadend = {false, false, false, false}
    shuffle(d)
    for i, dirnum in ipairs(d) do
      local xx = x + dirs[dirnum].x
      local yy = y + dirs[dirnum].y
      if map[yy] and map[yy][xx] then
          map[avg(y, yy)][avg(x, xx)] = false
          len = len + 1
          -- print(maze2string({width=w, height=h, data=map}))
          walk(xx, yy)
          len = len - 1
      else
        deadend[i] = true
      end
    end

    local dead = true
    for _, v in ipairs(deadend) do
      dead = dead and v
    end

    if dead then
      allDeads[#allDeads+1] = {xpos=x, ypos=y, length=len}
      len = 0
    end

  end
  
  local sX = math.random(1, w)*2
  local sY = math.random(1, h)*2
  
  walk(sX, sY)

  map[sY][sX] = "S"

  local maxLen = 0
  local endN = 0

  for i,v in ipairs(allDeads) do
    -- print(i,v["length"])
    if v["length"] > maxLen then
      endN = i
    end
  end

  local eX = allDeads[endN]["xpos"]
  local eY = allDeads[endN]["ypos"]

  map[eY][eX] = "F"
  
  return {width=w, height=h, data=map, start={x=sX,y=sY}, finish={x=eX,y=eY}}
end

function maze2string(map)
    local h = map["height"]
    local w = map["width"]
    local data = map["data"]

    local s = {}
    for i = 1, h*2+1 do
        for j = 1, w*2+1 do
            if data[i][j] == "S" then
                table.insert(s, 'S')
            elseif data[i][j] == "F" then
                table.insert(s, 'F')
            elseif data[i][j] then
                table.insert(s, '#')
            else
                table.insert(s, ' ')
            end
        end
    table.insert(s, '\n')
    end

    for _, p in ipairs(players) do
        local place = p["pos"]["x"] + (p["pos"]["y"]-1) * (w*2+1+1) 
        -- print (p["pos"]["x"],p["pos"]["y"],place)
        s[place] = tostring(p["name"])
    end

    return table.concat(s)
end

-- for i=1,10 do
--   print(i)
--   print(maze2string(make_maze(4,4)))
-- end
-- print(maze2string(make_maze(32,8)))
-- print(maze2string(make_maze(4,4)))
-- print(maze2string(make_maze(6,3)))

-- maze = make_maze(6,3)
-- print(maze2string(maze))
-- print(scan(maze["start"]))
-- print(getInfo())
-- prepareGame({4})
-- startGame()
-- print(maze2string(maze))

-- moves = {"INFO","SCAN","MOVE 1 0","MOVE 1 0","MOVE 1 0","MOVE 1 0"}

-- for i,m in ipairs(moves) do
--     print(handlePlayerMove(4, m))
--     playRound()
-- end