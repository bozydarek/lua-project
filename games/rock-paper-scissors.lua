gameName = "ROCK, PAPER, SCISSORS"
shortGameName = "RPS"
minimumNumberOfPlayers = 2
maximumNumberOfPlayers = 5

local players = {}
local moves = {"PASS", "ROCK", "PAPER", "SCISSORS" }

local maxRounds = 5
local roundNumber = 1

function prepareGame(listOfPlayersIDs)
    for _, v in ipairs(listOfPlayersIDs) do
        players[#players+1] = {move="", name=v, score=0, waiting = false}
    end
end

function startGame() -- additional function to handle evrething that have to be handle after prepareGame but before first round

end


function handlePlayerMove(playerID, move)
    for _, v in ipairs(players) do
        if playerID == v["name"] then
            if move == "WAIT" then
                if v["move"] == "" then
                    v["move"] = "PASS"
                end
                v["waiting"] = true;
                return "OK"
            elseif move == "SCORE" then
                return tostring(v["score"])
            elseif move == "ROUND" then
                return tostring(roundNumber)
            else
                for _, m in ipairs(moves) do
                    if m == move then
                        if v["move"] == "" then
                            v["move"] = move
                            return "OK"
                        end
                        return "WRONG MOVE"
                    end
                end
            end
            return "UNKNOWN COMMAND"
        end
    end
    return "UNKNOWN PLAYER"
end

local function assignPoints(winMove, losers, playersWhoPass )
    for _, v in ipairs(players) do
        if v["move"] == winMove then
            v["score"] = v["score"] + losers + math.ceil(playersWhoPass/2)
        end
    end
end

local function penaltyPoints(loserMove)
    for _, v in ipairs(players) do
        if v["move"] == loserMove then
            v["score"] = v["score"] - 1
        end
    end
end

function playRound()
    roundNumber = roundNumber + 1
    for _, v in ipairs(players) do
        if v["move"] == "" then
            v["move"] = "PASS"
        end
    end

    -- handle points
    local moveCounter = {}
    for _, m in ipairs(moves) do
        moveCounter[m] = 0
    end

    for _, v in ipairs(players) do
        moveCounter[v["move"]] = moveCounter[v["move"]] + 1
    end

    if moveCounter["ROCK"]*moveCounter["PAPER"]*moveCounter["SCISSORS"] ~= 0 then
        -- pass, no one gets points
    else
        if moveCounter["ROCK"] == 0 then
            if moveCounter["PAPER"]*moveCounter["SCISSORS"] == 0 then
                assignPoints("SCISSORS",moveCounter["PAPER"],moveCounter["PASS"])
                assignPoints("PAPER",moveCounter["SCISSORS"],moveCounter["PASS"])
            else
                assignPoints("SCISSORS",moveCounter["PAPER"],moveCounter["PASS"])
                penaltyPoints("PAPER")
            end
        elseif moveCounter["PAPER"] == 0 then
            if moveCounter["ROCK"]*moveCounter["SCISSORS"] == 0 then
                assignPoints("SCISSORS",moveCounter["ROCK"],moveCounter["PASS"])
                assignPoints("ROCK",moveCounter["SCISSORS"],moveCounter["PASS"])
            else
                assignPoints("ROCK",moveCounter["SCISSORS"],moveCounter["PASS"])
                penaltyPoints("SCISSORS")
            end
        elseif moveCounter["SCISSORS"] == 0 then
            if moveCounter["PAPER"]*moveCounter["ROCK"] == 0 then
                assignPoints("PAPER",moveCounter["ROCK"],moveCounter["PASS"])
                assignPoints("ROCK",moveCounter["PAPER"],moveCounter["PASS"])
            else
                assignPoints("PAPER",moveCounter["ROCK"],moveCounter["PASS"])
                penaltyPoints("ROCK")
            end
        end
    end

    -- prepare before next round
    for _, v in ipairs(players) do
        v["move"] = ""
    end
end

function getScore()
    local res = {}
    for _, v in ipairs(players) do
        res[v["name"]] = v["score"]
    end
    return res
end

function notify()
    local res = {}
    for _, v in ipairs(players) do
        if v["waiting"] then
            res[v["name"]] = "OK"
            v["waiting"] = false
        end
    end
    return res
end

function isGameOver()
    return (roundNumber > maxRounds)
end

function getView()
    return "No view available"
end

function handleAdminFunctions(message)
    return "No admin functions available"
end

--prepareGame({1,2,3})
--print(handlePlayerMove(1,"ROCK"))
--print(handlePlayerMove(2,"ROCK"))
--print(handlePlayerMove(3,"ROCK"))
--print(handlePlayerMove(3,"ROCK"))
--
--playRound()
--for _, v in ipairs(players) do
--    io.write(v["score"], ",")
--end
--print()
--
--print(handlePlayerMove(1,"ROCK"))
--print(handlePlayerMove(2,"ROCK"))
--print(handlePlayerMove(3,"PAPER"))
--print(handlePlayerMove(4,"PAPER"))
--
--playRound()
--for _, v in ipairs(players) do
--    io.write(v["score"], ",")
--end
--print()
--
--
-- --print(handlePlayerMove(1,"ROCK")) -- auto pass
--print(handlePlayerMove(2,"PASS"))
--print(handlePlayerMove(3,"PAPER"))
--
--playRound()
--for _, v in ipairs(players) do
--    io.write(v["score"], ",")
--end
--print()
