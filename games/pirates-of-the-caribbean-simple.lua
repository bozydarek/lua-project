score = 100
bonus = 2.5
scoreMultByBonus = score * bonus
hide = true;
local intVar = 42;

gameName = "Pirates of the caribbean (simple version)"
shortGameName = "PC"
minimumNumberOfPlayers = 2
maximumNumberOfPlayers = 4

local rumNumber = 0

local players = {}
--local moves = {"NONE", "MAP", "MOVE", "RUM", "WAIT"}

function prepareGame(listOfPlayersIDs)
    for _, v in ipairs(listOfPlayersIDs) do
        players[#players+1] = {pos={x=0,y=0}, newpos={x=0,y=0}, name=v, score=0, rum=100, canMove=true, waiting=false}
    end
end

local map = { h=10, w=10, data={} }
local lastMAP = ""

function startGame()
    math.randomseed( os.time() )

    for i, v in ipairs(players) do

        if i == 1 then  v["pos"] = {x=1,y=1}
        elseif i == 2 then v["pos"] = {x=1,y=10}
        elseif i == 3 then v["pos"] = {x=10,y=1}
        elseif i == 4 then v["pos"] = {x=10,y=10}
        end                
    end

    for x=1, map["w"] do
        map["data"][x] = {}
        for y=1, map["h"] do
            map["data"][x][y] = 0
            if x > 1 and math.random( 10 ) > 7 then
                map["data"][x][y] = math.random( 20, 50 )
                rumNumber = rumNumber + 1
            end
        end
    end

    

    lastMAP = prepareMap()
end

local function getXY(move)
    local res = {}
    for i in string.gmatch(move, "%S+") do
        res[#res+1] = i
    end

    return tonumber(res[2]), tonumber(res[3])
end

function prepareMap()
    local res = "" .. tostring(rumNumber + #players) .. "\n"
    
    for _, v in ipairs(players) do
        res = res .. "P".. v["name"] .. " " .. v["pos"]["x"] .. " " .. v["pos"]["y"] .. "\n"
    end

    for x=1, map["w"] do
        for y=1, map["h"] do
            if map["data"][x][y] > 0 then
                res = res .. "R " .. tostring(x) .. " " .. tostring(y) .. " " .. tostring( map["data"][x][y]) .. "\n"
            end
        end
    end
    
    return res
end

local function copyPos(dest, source)
    dest["x"] = source["x"]
    dest["y"] = source["y"]
end

local function IsPosEq(dest, source)
    return dest["x"] == source["x"] and dest["y"] == source["y"]
end

local function checkBorders( playerNewPos )
    return  playerNewPos["x"] > 0 and  playerNewPos["x"] <= map["w"]
        and playerNewPos["y"] > 0 and  playerNewPos["y"] <= map["h"]
end

function handlePlayerMove(playerID, move)
    for _, v in ipairs(players) do
        if playerID == v["name"] then
            if move:sub(1,4) == "MOVE" then
                if v["canMove"] then
                    local x,y = getXY(move)
                    if type(x) == "number" and type(y) == "number" then
                        if math.abs(x) <= 1 and math.abs(y) <= 1 then
                            v["canMove"] = false;
                            v["newpos"]["x"] = v["pos"]["x"] + x
                            v["newpos"]["y"] = v["pos"]["y"] + y

                            if checkBorders(v["newpos"]) then
                                return "OK"
                            else
                                copyPos(v["newpos"],v["pos"])

                                return "Wrong move (out off the map)"
                            end
                        end
                    end
                    return "Wrong arguments to move"
                else
                    return "You can't move now."
                end
            elseif move == "MAP" then
                return lastMAP
            elseif move == "RUM" then
                return tostring(v["rum"])
            elseif move == "WAIT" then
                v["waiting"] = true
                return "OK"
            else
                return "WRONG MOVE"
            end

            return "UNKNOWN COMMAND"
        end
    end
    return "UNKNOWN PLAYER"
end

function playRound()
    for i=1,#players do
        for j=i+1,#players do
            if IsPosEq(players[i]["newpos"], players[j]["newpos"]) then
                copyPos(players[i]["newpos"],players[i]["pos"])
                copyPos(players[j]["newpos"],players[j]["pos"])
                
                -- TODO: return "colision" in notify ?
            end
        end
    end

    for _, v in ipairs(players) do
        copyPos(v["pos"],v["newpos"])
        v["rum"] = v["rum"] - 1

        local pos = v["pos"]
        if map["data"][pos["x"]][pos["y"]] > 0 then
            v["score"] =  v["score"] + map["data"][pos["x"]][pos["y"]]
            v["rum"] = v["rum"] + map["data"][pos["x"]][pos["y"]]
            map["data"][pos["x"]][pos["y"]] = 0
            rumNumber = rumNumber - 1
        end

        if v["rum"] > 0 then
            v["canMove"] = true
        end
    end

    lastMAP = prepareMap()
end

function getScore()
    local res = {}
    for _, v in ipairs(players) do
        res[v["name"]] = v["score"]
    end
    return res
end

function notify()
    local res = {}
    for _, v in ipairs(players) do
        if v["waiting"] then
            res[v["name"]] = "OK"
            v["waiting"] = false
        end
    end
    return res
end

function isGameOver()

    local playersCantMove = true
    for _, v in ipairs(players) do
        if v["rum"] ~= 0 then
            playersCantMove = false
        end
    end

    if playersCantMove then
        return true
    end

    for x=1, map["w"] do
        for y=1, map["h"] do
            if map["data"][x][y] > 0 then
                return false
            end
        end
    end

    return true
end

function getView()
    return lastMAP
end

function handleAdminFunctions(message)
    return "No admin functions available"
end

-- prepareGame({1,2,3})
-- startGame()

-- print(handlePlayerMove(1,"MAP"))

-- print(handlePlayerMove(1,"MOVE 1 0"))
-- print(handlePlayerMove(2,"MOVE 1 1"))
-- print(handlePlayerMove(3,"MOVE -1 2"))
-- print(handlePlayerMove(3,"MOVE -1 1"))
-- print(handlePlayerMove(3,"MOVE 1 1"))

-- playRound()

-- print(handlePlayerMove(1,"MAP"))


-- print(handlePlayerMove(1,"MOVE 0 -1"))
-- print(handlePlayerMove(2,"RUM"))
-- print(handlePlayerMove(2,"MOVE 0 1"))
-- print(handlePlayerMove(2,"WAIT"))

-- playRound()

-- print(handlePlayerMove(1,"MAP"))

-- for _, v in ipairs(players) do
--    io.write(v["score"], ",")
-- end
-- print()
