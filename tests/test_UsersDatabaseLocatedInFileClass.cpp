#include <cstdio>
#include <fstream>
#include "gtest/gtest.h"

#include "../src/usersdatabaselocatedinfile.h"

using namespace std;

class usersDatabaseLocatedInFileClassTest : public ::testing::Test
{
protected:
    usersDatabaseLocatedInFileClassTest()
    {
        ofstream fileStream("testDatabase.tmp");
        fileStream<<"0 login password 1"<<endl;
        fileStream<<"1 aaa pas 1"<<endl;
        fileStream<<"2 foo bar 2"<<endl;
        fileStream<<"3 admin tajnehaslo 0"<<endl;

        fileStream.close();

        users = new UsersDatabaseLocatedInFile("testDatabase.tmp");
    }

    virtual ~usersDatabaseLocatedInFileClassTest() {
        remove( "testDatabase.tmp" );
        delete users;
    }

    UsersDatabaseLocatedInFile *users;
};

TEST_F(usersDatabaseLocatedInFileClassTest, MethodUserExistsReturnTrue)
{
    bool res = users->userExists("login");
    EXPECT_EQ(true, res);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodUserExistsReturnFalse)
{
    bool res = users->userExists("test");
    EXPECT_EQ(false, res);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodPasswordCorrectReturnTrue)
{
    bool res = users->passwordCorrect("login", "password");
    EXPECT_EQ(true, res);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodPasswordCorrectReturnFalse)
{
    bool res = users->passwordCorrect("login", "wrongPassword");
    EXPECT_EQ(false, res);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodGetUserIdReturnId)
{
    int res;
    try
    {
        res = users->getUserId("login");
    }
    catch(exception& e)
    {
        res = -1;
    }
    EXPECT_EQ(0, res);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodGetUserIdThrowsException)
{
    ASSERT_THROW(users->getUserId("loginDontExists"), runtime_error);
}

TEST_F(usersDatabaseLocatedInFileClassTest, MethodGetUserTypeReturnProperUserType)
{
    std::string login[] = {"login", "aaa", "foo", "admin"};
    UserType type[] = {UserType::User, UserType::User, UserType::Observer, UserType::Admin};
    
    for(int i=0; i<=3; ++i)
    {
        try
        {
            EXPECT_EQ(type[i], users->getUserType(login[i]));
        }
        catch(...){
            FAIL() << "We shouldn't get here.";
        }
    }
}