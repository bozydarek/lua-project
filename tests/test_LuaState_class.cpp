#include <iostream>
#include <fstream>
#include "gtest/gtest.h"

#include "../src/LuaState.h"

class LuaStateCleanClass : public ::testing::Test {
protected:
    LuaState lState;

    LuaStateCleanClass(){
        std::ofstream tg("./testGame.lua");
        std::ofstream nlf("./notLuaFile.txt");
    }

    virtual ~LuaStateCleanClass(){
        remove("./testGame.lua");
        remove("./notLuaFile.txt");
    }
};

TEST_F(LuaStateCleanClass, LuaStateTryToLoadFakeFile){
    bool result = lState.load("./notExistingFile.lua");
    EXPECT_EQ(false, result);
}

TEST_F(LuaStateCleanClass, LuaStateTryToLoadNoLuaFile){
    bool result = lState.load("./notLuaFile.txt");
    EXPECT_EQ(false, result);
}

TEST_F(LuaStateCleanClass, LuaStateTryToLoadTestGameFile){
    bool result = lState.load("./testGame.lua");
    EXPECT_EQ(true, result);
}

TEST_F(LuaStateCleanClass, LuaStateTestIfGetErrMsgIsEmptyAtBegin){
    EXPECT_EQ("",lState.getLastError());
}

class LuaStateAfterLoad_testGame : public ::testing::Test {
protected:
    LuaState lState;

    LuaStateAfterLoad_testGame(){
        std::ofstream out("testGame.lua");
        out << "score = 100" << std::endl;
        out << "bonus = 2.5" << std::endl;
        out << "scoreMultByBonus = score * bonus" << std::endl;
        out << "hide = true;" << std::endl;
        out << "list = {\"Ala\", \"Ma\", \"Kota\"}" << std::endl;
        out << "int_list = {0,1,2}" << std::endl;
        out << "double_list = {1.1, 2.2, 3.14}" << std::endl;

        out << "gameName = \"Test Game!\"" << std::endl;


        out << "function testFun(bool, int)\n"
                "   print(\" LUA-TEST-FUN:\",bool,int)\n"
                "end" << std::endl;

        out << "function adder(a, b)\n"
                "   return a+b\n"
                "end" << std::endl;
        
        out << "function conc(a, b)\n"
//                "   print(a,b)\n"
                "   return a..b\n"
                "end" << std::endl; 

        out << "function mixer(w1, w2)\n"
                "   return w1:sub(1, math.floor(w1:len()/2)+1), w2:sub(math.floor(w2:len()/2)+1, w2:len())\n"
                "end" << std::endl;

        out << "function arraySum(list)\n"
                "    local res = 0\n"
                "    for _,v in ipairs(list) do\n"
                "        res = res + v\n"
                "    end\n"
                "    return res\n"
                "end" << std::endl;

        out << "function splitter(word)\n"
                "    local res = {}\n"
                "    for i in string.gmatch(word, \".\") do\n"
                "        res[#res+1] = i\n"
                "    end\n"
                "    return res\n"
                "end" << std::endl;

        out << "mapTest = {x=1, y=2, z=5}" << std::endl;
        out << "mapTestDouble = {x=1.1, y=2.2, z=5.5, [42]=42.2, [2]=222.22}"<< std::endl;

        lState.load("./testGame.lua");
    }
    
    virtual ~LuaStateAfterLoad_testGame(){
        remove("./testGame.lua");
    }
};

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadExistingInt){
    int result = lState.getGlobalInt("score");
    EXPECT_EQ(100, result);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadNotExistingInt){
    ASSERT_THROW(lState.getGlobalInt("NOTscore"), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadExistingDouble){
    double result = lState.getGlobalDouble("bonus");
    EXPECT_EQ(2.5, result);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadExistingString){
    std::string result = lState.getGlobalString("gameName");
    EXPECT_EQ("Test Game!", result);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadExistingBool){
    bool result = lState.getGlobalBool("hide");
    EXPECT_EQ(true, result);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadNotTableAsSeqList){
    ASSERT_THROW(lState.getGlobalSeqListOfStringAsVector("score"), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadStringTable){
    std::vector<std::string> res = lState.getGlobalSeqListOfStringAsVector("list");
    ASSERT_EQ(3, res.size());

    std::string expected[3] = {"Ala", "Ma", "Kota"};
    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(expected[i], res[i]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadIntTable){
    std::vector<int> res = lState.getGlobalSeqListOfIntAsVector("int_list");
    ASSERT_EQ(3, res.size());

    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(i, res[i]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadDoubleTable){
    std::vector<double> res = lState.getGlobalSeqListOfDoubleAsVector("double_list");
    ASSERT_EQ(3, res.size());

    double expected[3] = {1.1, 2.2, 3.14};
    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(expected[i], res[i]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadDoubleTableAsIntTable){
    ASSERT_THROW(lState.getGlobalSeqListOfIntAsVector("double_list"), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadStringTableAsIntTable){
    ASSERT_THROW(lState.getGlobalSeqListOfIntAsVector("list"), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseUndefinedFunction) {
    ASSERT_THROW(lState.callFunction("undefinedFunction", 2, 1, 3, 42), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseVariableAsFunction) {
    ASSERT_THROW(lState.callFunction("score", 0, 0), GetGlobalError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseCallWithoutTooManyParameters){
    ASSERT_THROW(lState.callFunction("testFun", 0, 0, true, 42), CallFunctionError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseCallWithLessParametersThenNeeded){
    ASSERT_THROW(lState.callFunction("testFun", 10, 0, true, 42), CallFunctionError);
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToPrepareStackBeforePcall){
    lState.callFunction("testFun", 2, 0, true, 42);
    //lState.debug();
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTestPushToStack){
    lState.pushToStack(1);
    lState.pushToStack(1.1);
    lState.pushToStack(false);
    lState.pushToStack(std::string("ABC"));
    lState.pushToStack("QWE");
    // lState.debug();
    EXPECT_EQ("QWE", lState.getStringFromStack(-1));
    EXPECT_EQ("ABC", lState.getStringFromStack(-1));
    EXPECT_EQ(false, lState.getBoolFromStack(-1));
    EXPECT_EQ(1.1, lState.getDoubleFromStack(-1));
    EXPECT_EQ(1, lState.getIntFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToCallFunctionToAdd5And42AndGrabOutput){
    lState.callFunction("adder", 2, 1, 5, 42);
    EXPECT_EQ(5+42, lState.getIntFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToCallFunctionToAddNumbersAndGrabOutput){
    lState.callFunction("adder", 2, 1, 5.5, 1.3);
    EXPECT_EQ(5.5+1.3, lState.getDoubleFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToCallFunctionToAddHalfAndTwoAndGrabOutput){
    lState.callFunction("adder", 2, 1, 0.5, 2);
    EXPECT_EQ(0.5+2, lState.getDoubleFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseLuaFunctionToConcatStrings){
    lState.callFunction("conc", 2, 1, std::string("ABC"), std::string("QWERTY"));
    EXPECT_EQ("ABCQWERTY", lState.getStringFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseLuaFunctionToConcatConstChar){
    lState.callFunction("conc", 2, 1, "ABC", "QWERTY");
    EXPECT_EQ("ABCQWERTY", lState.getStringFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseLuaFunctionWithStrings){
    lState.callFunction("mixer", 2, 2, std::string("ABC"), std::string("QWERTY"));
    //lState.debug();
    EXPECT_EQ("RTY", lState.getStringFromStack(-1));
    EXPECT_EQ("AB", lState.getStringFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToPushVectorAsListOnStack){
    std::vector<int> list {1,2,3,4,5,6,7,8,9,10};
    lState.pushToStack(list);
    auto recv = lState.getListOfIntFromStackAsVector(-1);

    int size = (int) list.size();
    for (int i = 0; i < size; ++i) {
        EXPECT_EQ(list[i], recv[i]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseLuaFunctionToSumNumbersInList){
    std::vector<double> list {1.1, 2.2, 3.3};
    lState.callFunction("arraySum", 1, 1, list);
    EXPECT_EQ(6.6, lState.getDoubleFromStack(-1));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToUseLuaFunctionToSplitStringToList){
    std::vector<std::string> list {"A","B","C","D"};
    lState.callFunction("splitter", 1, 1, "ABCD");
    auto recv = lState.getListOfStringFromStackAsVector(-1);

    int size = (int) list.size();
    for (int i = 0; i < size; ++i) {
        EXPECT_EQ(list[i], recv[i]);
    }
}


TEST_F(LuaStateAfterLoad_testGame, LuaStateCheckIfFunctionExist) {
    EXPECT_EQ(true, lState.checkIfFunctionExist("splitter"));
    EXPECT_EQ(false, lState.checkIfFunctionExist("NOTsplitter"));
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadGloblaMapOfInt){
    std::map<std::string, int> map = {{"x",1},{"y",2},{"z",5}};
    auto recv = lState.getGlobalMapOfInt("mapTest");

    for(auto i : map) {
        EXPECT_EQ(1, recv.count(i.first));
        EXPECT_EQ(i.second, recv[i.first]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadGloblaMapOfDouble){
    std::map<std::string, double> map = {{"x",1.1},{"y",2.2},{"z",5.5}};
    auto recv = lState.getGlobalMapOfDouble("mapTestDouble");

    for(auto i : map) {
        EXPECT_EQ(1, recv.count(i.first));
        EXPECT_EQ(i.second, recv[i.first]);
    }
}

TEST_F(LuaStateAfterLoad_testGame, LuaStateTryToLoadGloblaIntMapOfDouble){
    std::map<int, double> map = {{42, 42.2},{2, 222.22}};
    auto recv = lState.getGlobalIntMapOfDouble("mapTestDouble");

    for(auto i : map) {
        EXPECT_EQ(1, recv.count(i.first));
        EXPECT_EQ(i.second, recv[i.first]);
    }
}