#include <iostream>
#include "gtest/gtest.h"

#include "../src/ThreadSafeQueue.h"

TEST(ThreadSafeQueueTest,MethodSizeReturns0) {
    ThreadSafeQueue<int> queue;

    int res = queue.size();
    EXPECT_EQ(0, res);
}

TEST(ThreadSafeQueueTest,MethodSizeReturnsNot0) {
    ThreadSafeQueue<int> queue;

    queue.push(1);

    int res = queue.size();
    EXPECT_EQ(1, res);
}

TEST(ThreadSafeQueueTest,MethodEmptyReturnsTrue) {
    ThreadSafeQueue<int> queue;

    bool res = queue.empty();
    EXPECT_EQ(true, res);
}

TEST(ThreadSafeQueueTest,MethodEmptyReturnsFalse) {
    ThreadSafeQueue<int> queue;

    queue.push(1);

    bool res = queue.empty();
    EXPECT_EQ(false, res);
}


TEST(ThreadSafeQueueTest,MethodPopReturnsPushedElement) {
    ThreadSafeQueue<int> queue;

    int element = 1;

    queue.push(element);
    queue.push(2);

    bool serverShouldEnd = false;

    int res = queue.pop(ref(serverShouldEnd));
    EXPECT_EQ(element, res);
}
