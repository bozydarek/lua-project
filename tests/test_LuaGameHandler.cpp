#include <iostream>
#include <fstream>
#include "gtest/gtest.h"

#include "../src/LuaGameHandler.h"

class LuaGameHandlerFix : public ::testing::Test {
protected:
    LuaGameHandler gameHandler;

    LuaGameHandlerFix(){
        std::ofstream tg("./luaTestGame.lua");

        tg <<   "gameName = \"Test Game!\"\n"
                "shortGameName = \"TG\"\n"
                "minimumNumberOfPlayers = 2\n"
                "maximumNumberOfPlayers = 4\n"
                "function prepareGame(listOfPlayersIDs)\n"
                "   \n"
                "end\n"
                "\n"
                "function startGame()\n"
                "   \n"
                "end\n"
                "\n"
                "function handlePlayerMove(user, move)\n"
                "   return \"OK\"\n"
                "end\n"
                "\n"
                "function playRound()\n"
                "    \n"
                "end\n"
                "\n"
                "function getScore()\n"
                "    return {}\n"
                "end\n"
                ""
                "function notify()\n"
                "    return {}\n"
                "end\n"
                ""
                "function isGameOver()\n"
                "    return false\n"
                "end\n"
                "\n"
                "function getView()\n"
                "    return \"No view available\"\n"
                "end\n"
                "\n"
                "function handleAdminFunctions(message)\n"
                "    return \"No admin functions available\"\n"
                "end"  << std::endl;


        std::ofstream ntg("./luaTestGameWithoutFunctionPrepareGame.lua");

        ntg <<  "gameName = \"Test Game!\"\n"
                "shortGameName = \"TG\"\n"
                "minimumNumberOfPlayers = 2\n"
                "maximumNumberOfPlayers = 4\n"

                "function startGame()\n"
                "   \n"
                "end\n"
                "function playRound()\n"
                "    \n"
                "end" << std::endl;

        std::ofstream ntg2("./luaTestGameWithoutVariableGameName.lua");

        ntg2 << "function prepareGame(listOfPlayersIDs)\n"
                "   \n"
                "end\n"
                "\n"
                "function startGame()\n"
                "   \n"
                "end\n"
                "\n"
                "function handlePlayerMove(user, move)\n"
                "   \n"
                "end\n"
                "\n"
                "function playRound()\n"
                "    \n"
                "end\n"
                "\n"
                "function getScore()\n"
                "    return {}\n"
                "end\n"
                ""
                "function notify()\n"
                "    return {}\n"
                "end\n"
                ""
                "function isGameOver()\n"
                "    return false\n"
                "end\n"
                "\n"
                "function getView()\n"
                "    return \"No view available\"\n"
                "end\n"
                "\n"
                "function handleAdminFunctions(message)\n"
                "    return \"No admin functions available\"\n"
                "end"  << std::endl;
    }

    virtual ~LuaGameHandlerFix(){
        remove("./luaTestGame.lua");
        remove("./luaTestGameWithoutVariableGameName.lua");
        remove("./luaTestGameWithoutFunctionPrepareGame.lua");
    }
};

TEST_F(LuaGameHandlerFix, loadTheGame){
    EXPECT_EQ(true, gameHandler.load("./luaTestGame.lua"));
    EXPECT_EQ("Test Game!", gameHandler.getFullName());
    EXPECT_EQ("TG", gameHandler.getShortName());
}

TEST_F(LuaGameHandlerFix, loadError){
    EXPECT_EQ(false, gameHandler.load("./luaTestGameWithoutFunctionPrepareGame.lua"));
    EXPECT_EQ("Problem with functions", gameHandler.getLastLoadError());
}
TEST_F(LuaGameHandlerFix, loadError2){
    EXPECT_EQ(false, gameHandler.load("./luaTestGameWithoutVariableGameName.lua"));
    EXPECT_EQ("Problem with constants", gameHandler.getLastLoadError());
}

class LuaGameHandlerAfterLoad : public ::testing::Test {
protected:
    LuaGameHandler gameHandler;

    LuaGameHandlerAfterLoad(){
        std::string name = "./luaTestGame.lua";
        std::ofstream tg(name);
        tg <<   "gameName = \"Test Game!\"\n"
                "function prepareGame(listOfPlayersIDs)\n"
                "   print(\"Players no.:\", #listOfPlayersIDs)\n"
                "   for i,v in ipairs(listOfPlayersIDs) do\n"
                "       print(\"*\",i,v)\n"
                "   end\n"
                "end\n"
                "\n"
                "function startGame()\n"
                "   \n"
                "end\n"
                "\n"
                "function handlePlayerMove(user, move)\n"
                "   return \"OK\"\n"
                "end\n"
                "\n"
                "function playRound()\n"
                "    \n"
                "end\n"
                "\n"
                "function getScore()\n"
                "    return {}\n"
                "end\n"
                ""
                "function notify()\n"
                "    return {}\n"
                "end\n"
                ""
                "function isGameOver()\n"
                "    return false\n"
                "end\n"
                "\n"
                "function getView()\n"
                "    return \"No view available\"\n"
                "end\n"
                "\n"
                "function handleAdminFunctions(message)\n"
                "    return \"No admin functions available\"\n"
                "end"  << std::endl;

        gameHandler.load(name);
    }

    virtual ~LuaGameHandlerAfterLoad(){
        remove("./luaTestGame.lua");
    }
};

TEST_F(LuaGameHandlerAfterLoad, TryToCallPrepareGameFunction){
    std::vector<int> playersIDs {1, 5, 42};
    gameHandler.prepareGame(playersIDs);
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1,"MOVE"));
}


class LuaGameHandlerAfterLoadAndPrepareRPSFromFile : public ::testing::Test {
protected:
    LuaGameHandler gameHandler;
    std::vector<int> playersIDs {1, 5, 42};

    LuaGameHandlerAfterLoadAndPrepareRPSFromFile(){
        assert(gameHandler.load("../games/rock-paper-scissors.lua"));
        gameHandler.prepareGame(playersIDs);
    }

    virtual ~LuaGameHandlerAfterLoadAndPrepareRPSFromFile(){

    }
};

TEST_F(LuaGameHandlerAfterLoadAndPrepareRPSFromFile, CheckGameShortName){
    EXPECT_EQ("RPS", gameHandler.getShortName());
}

TEST_F(LuaGameHandlerAfterLoadAndPrepareRPSFromFile, TryToDoSomeMoves){
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "ROCK"));
    EXPECT_EQ("UNKNOWN PLAYER\n", gameHandler.handlePlayerMove(2, "ROCK"));
    EXPECT_EQ("UNKNOWN COMMAND\n", gameHandler.handlePlayerMove(5, "TURN LEFT"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("WRONG MOVE\n", gameHandler.handlePlayerMove(5, "ROCK"));
}

TEST_F(LuaGameHandlerAfterLoadAndPrepareRPSFromFile, TryToDoSomeMovesPlayRoundAndMoveAgainAndThenTryToGetScore){
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "ROCK"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "ROCK"));

    gameHandler.playRound();

    auto res = gameHandler.getScore();
    EXPECT_EQ(-1, res[1]);
    EXPECT_EQ(2, res[5]);
    EXPECT_EQ(-1, res[42]);

    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "SCISSORS"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "ROCK"));

    gameHandler.playRound();

    res = gameHandler.getScore();
    EXPECT_EQ(-1, res[1]);
    EXPECT_EQ(2, res[5]);
    EXPECT_EQ(-1, res[42]);
}

TEST_F(LuaGameHandlerAfterLoadAndPrepareRPSFromFile, TryToPlaySomeMovesAndCheckScore){
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "ROCK"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "ROCK"));

    gameHandler.playRound();

    auto res = gameHandler.getScore();
    EXPECT_EQ(-1, res[1]);
    EXPECT_EQ(2, res[5]);
    EXPECT_EQ(-1, res[42]);

    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "ROCK"));

    gameHandler.playRound();

    res = gameHandler.getScore();
    EXPECT_EQ(0, res[1]);
    EXPECT_EQ(3, res[5]);
    EXPECT_EQ(-2, res[42]);

    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "SCISSORS"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "PAPER"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "PAPER"));

    gameHandler.playRound();

    res = gameHandler.getScore();
    EXPECT_EQ(2, res[1]);
    EXPECT_EQ(2, res[5]);
    EXPECT_EQ(-3, res[42]);

    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(1, "ROCK"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(5, "ROCK"));
    EXPECT_EQ("OK\n", gameHandler.handlePlayerMove(42, "PAPER"));

    gameHandler.playRound();

    res = gameHandler.getScore();
    EXPECT_EQ(1, res[1]);
    EXPECT_EQ(1, res[5]);
    EXPECT_EQ(-1, res[42]);

    EXPECT_EQ(false, gameHandler.isGameOver());
}

