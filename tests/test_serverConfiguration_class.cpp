#include <iostream>
#include <fstream>
#include "gtest/gtest.h"

#include "../src/serverConfiguration.h"

class serverConfigurationClassFixture : public ::testing::Test {
protected:
    serverConfiguration &conf = serverConfiguration::getConfiguration();
};

TEST_F(serverConfigurationClassFixture, checkIfThisClassRealyIsASingleton){
    EXPECT_EQ(true, (&serverConfiguration::getConfiguration()) == (&serverConfiguration::getConfiguration()));
    EXPECT_EQ(true, (&serverConfiguration::getConfiguration()) == (&conf));
}

TEST_F(serverConfigurationClassFixture, checkIfDefaultConfigWorksAtStart ){
    EXPECT_EQ("", conf.getGame_dir());
    EXPECT_EQ(true, conf.getGames_files().empty());
    EXPECT_EQ(true, (conf.getPort() > 0));
    EXPECT_EQ("database.txt", conf.getDatabase_file());
    EXPECT_EQ(3000, conf.getRoundTimeInMilisec());
}

TEST_F(serverConfigurationClassFixture, checkConfigLoading){
    std::string fileName = "./serverConfigFile_test.lua";
    
    std::ofstream confFile(fileName);
    confFile << "port=6666" << std::endl;
    confFile << "gameDir=\"games/\"" << std::endl;
    confFile << "gamesFiles={\"tictactoe.lua\"}" << std::endl;
    confFile << "userDatabaseFile=\"./database2.txt\"" << std::endl;
    confFile << "roundTime=500" << std::endl;
    confFile.close();

    conf.loadConfiguration(fileName);

    EXPECT_EQ(6666, conf.getPort());
    EXPECT_EQ("games/", conf.getGame_dir());
    EXPECT_EQ("tictactoe.lua", conf.getGames_files()[0]);
    EXPECT_EQ("games/tictactoe.lua", conf.getFullPathToGamesFiles()[0]);
    EXPECT_EQ("./database2.txt", conf.getDatabase_file());
    EXPECT_EQ(500, conf.getRoundTimeInMilisec());

    remove(fileName.c_str());
}
