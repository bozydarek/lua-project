#!/bin/bash

if [ $# == 0 ] ; then
    echo "./runBots <NUMBER OF BOTS>"
    exit 1
fi

if [ $DESKTOP_SESSION == "xubuntu" ] ; then
	for ((i=1; i<=$1; i++)); do
		mkfifo fifo$i ;
		xfce4-terminal -e "sh -ic \"python3.5 testRPSinfinit.py $i < fifo$i | nc localhost 4444 >fifo$i; exec bash\"" 
	done
elif [ $DESKTOP_SESSION == "ubuntu" ] ; then
	for ((i=1; i<=$1; i++)); do
		mkfifo fifo$i ;
		gnome-terminal -e "sh -ic \"python3.5 testRPSinfinit.py $i < fifo$i | nc localhost 4444 >fifo$i; exec bash\"" 
	done
else
	echo "Unsupported system"
	exit 1
fi

