require 'socket'

def distance(x1, y1, x2, y2)
  return Math.sqrt((x1-x2)**2 + (y1-y2)**2)
end

if(ARGV.size != 3)
  puts "Wrong number of arguments. Correct arguments: bot number, server adress, server port"
  exit
end

botNumber = ARGV[0]
serverAdress = ARGV[1]
serverPort = ARGV[2]

username = "user" + botNumber
password = "pass"

socket = TCPSocket.new serverAdress, serverPort

status = :LOGIN

map = ""

line = ""

positionX = 0
positionY = 0

rumX = 0
rumY = 0

while line = socket.gets.chomp

  puts "IN: " + line

  if line == 'GAME OVER'

    line = socket.gets.chomp
    puts "IN (powinno byc UNKNOWN COMMAND z uwagi na logike dzialania serwera): " + line

    socket.write 'SEARCH GAME'
    puts 'OUT: ' + 'SEARCH GAME'
    status = :IN_MATCHMAKER
  else
    if(status == :LOGIN)
      if line == 'LOGIN'
        socket.write username
        puts 'OUT: ' + username
        status = :PASSWORD
      else
        puts 'Expected login'
        socket.close
        exit
      end
    elsif status == :PASSWORD
      if line == 'PASSWORD'
        socket.write password
        puts 'OUT: ' + password
        status = :LOBBY
      else
        puts 'Expected password'
        socket.close
        exit
      end
    elsif status == :LOBBY
      if line == 'OK'
        socket.write 'JOIN GAME PC'
        puts 'OUT: ' + 'JOIN GAME PC'
        status = :IN_GAME
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME
      if line == 'OK'
        socket.write 'SEARCH GAME'
        puts 'OUT: ' + 'SEARCH GAME'
        status = :IN_MATCHMAKER
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_MATCHMAKER
      if line == 'OK'
        status = :IN_MATCHMAKER_START
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_MATCHMAKER_START
      if line == 'START'
        socket.write 'MAP'
        puts 'OUT: ' + 'MAP'
        status = :IN_GAME_ROOM_MAP
      else
        puts 'Expected start'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_MAP

        n = Integer(line)
        value = 0
        rumX = 0
        rumY = 0
        n.times do |i|
          line = socket.gets.chomp
          puts "IN: " + line

          arr = line.gsub(/\s+/m, ' ').strip.split(" ")

          if arr[0] == 'P'+ botNumber
            puts 'Moja pozycja ' + arr[1] + ' ' + arr[2]
            positionX = Integer(arr[1])
            positionY = Integer(arr[2])
          elsif arr[0] == 'R'
            tmpRumX = Integer(arr[1])
            tmpRumY = Integer(arr[2])
            tmpDist = distance(positionX, positionY, tmpRumX, tmpRumY)
            tmpValue = Integer(arr[3]) / tmpDist
            if tmpValue > value
              value = tmpValue
              rumX = tmpRumX
              rumY = tmpRumY
            end
          end
        end

        puts 'Najlepszy rum' + String(rumX) + ' ' + String(rumY)
        socket.write 'RUM'
        puts 'OUT: ' + 'RUM'
        status = :IN_GAME_ROOM_RUM
    elsif status == :IN_GAME_ROOM_RUM
        moveX = 0
        moveY = 0

        if positionX < rumX
          moveX = 1
        elsif positionX > rumX
          moveX = -1
        end

        if positionY < rumY
          moveY = 1
        elsif positionY > rumY
          moveY = -1
        end

        socket.write 'MOVE ' + String(moveX) + ' ' + String(moveY)
        puts 'OUT: ' + 'MOVE ' + String(moveX) + ' ' + String(moveY)
        status = :IN_GAME_ROOM_MOVE
    elsif status == :IN_GAME_ROOM_MOVE
      if line == 'OK'
        socket.write 'WAIT'
        puts 'OUT: ' + 'WAIT'
        status = :IN_GAME_ROOM_WAIT_1
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_WAIT_1
      if line == 'OK'
        status = :IN_GAME_ROOM_WAIT_2
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_WAIT_2
      if line == 'OK'
        socket.write 'MAP'
        puts 'OUT: ' + 'MAP'
        status = :IN_GAME_ROOM_MAP
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    end
  end
end


socket.close