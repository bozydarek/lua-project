#!/bin/bash

if [ $# == 0 ] ; then
    echo "./runBots <NUMBER OF BOTS>"
    exit 1
fi


for ((i=1; i<=$1; i++)); do
	mkfifo fifo$i ;
	python3.5 testRPSinfinit.py $i < fifo$i | nc localhost 4444 >fifo$i & 
done

