require 'socket'

if(ARGV.size != 3)
  puts "Wrong number of arguments. Correct arguments: bot number, server adress, server port"
  exit
end

botNumber = ARGV[0]
serverAdress = ARGV[1]
serverPort = ARGV[2]

username = "user" + botNumber
password = "pass"

socket = TCPSocket.new serverAdress, serverPort

status = :LOGIN

line = ""

while line = socket.gets.chomp

  puts "IN: " + line

  if line == 'GAME OVER'

    line = socket.gets.chomp
    puts "IN (powinno byc UNKNOWN COMMAND z uwagi na logike dzialania serwera): " + line

    socket.write 'SEARCH GAME'
    puts 'OUT: ' + 'SEARCH GAME'
    status = :IN_MATCHMAKER
  else
    if(status == :LOGIN)
      if line == 'LOGIN'
        socket.write username
        puts 'OUT: ' + username
        status = :PASSWORD
      else
        puts 'Expected login'
        socket.close
        exit
      end
    elsif status == :PASSWORD
      if line == 'PASSWORD'
        socket.write password
        puts 'OUT: ' + password
        status = :LOBBY
      else
        puts 'Expected password'
        socket.close
        exit
      end
    elsif status == :LOBBY
      if line == 'OK'
        socket.write 'JOIN GAME RPS'
        puts 'OUT: ' + 'JOIN GAME RPS'
        status = :IN_GAME
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME
      if line == 'OK'
        socket.write 'SEARCH GAME'
        puts 'OUT: ' + 'SEARCH GAME'
        status = :IN_MATCHMAKER
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_MATCHMAKER
      if line == 'OK'
        status = :IN_MATCHMAKER_START
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_MATCHMAKER_START
      if line == 'START'
        socket.write 'SCORE'
        puts 'OUT: ' + 'SCORE'
        status = :IN_GAME_ROOM_SCORE
      else
        puts 'Expected start'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_SCORE
        move = ["ROCK", "PAPER", "SCISSORS"].sample
        socket.write move
        puts "OUT: " + move
        status = :IN_GAME_ROOM_MOVE
    elsif status == :IN_GAME_ROOM_MOVE
      if line == 'OK'
        socket.write 'WAIT'
        puts 'OUT: ' + 'WAIT'
        status = :IN_GAME_ROOM_WAIT_1
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_WAIT_1
      if line == 'OK'
        status = :IN_GAME_ROOM_WAIT_2
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    elsif status == :IN_GAME_ROOM_WAIT_2
      if line == 'OK'
        socket.write 'SCORE'
        puts 'OUT: ' + 'SCORE'
        status = :IN_GAME_ROOM_SCORE
      else
        puts 'Expected ok'
        socket.close
        exit
      end
    end
  end
end


socket.close