import sys
import os
import random
#import networkx as nx

# $ mkfifo fifoRPS
# $ python3.5 testRPS.py <fifoRPS | nc localhost 4444 >fifoRPS

class Stan:
    pass


def Blad(s):
    raise (Exception(s))


def put(s):
    print("out:", s, file=sys.stderr)
    print(s)


def get():
    s = input();
    print("in:", s, file=sys.stderr)
    return s


def Get_ok(num=1):
    for i in range(num):
        OK = get()
        if OK != "OK":
            Blad("Gdzie jest ok?")


def LOGIN():
    login = "user"
    haslo = "pass"
    LOG = get()
    if LOG != "LOGIN":
        Blad("Panie, co jest grane, nie pytajo o login D:\n")
    
    if len(sys.argv) == 2:
        login += sys.argv[1]    
    
    put(login)
    PAS = get()
    if PAS != "PASSWORD":
        Blad("Panie, co jest grane, nie pytajo o haslo D:\n")
    put(haslo)
    Get_ok()

def JOINGAME(game):
    join = "JOIN GAME "+game
    put(join)
    Get_ok()


def SEARCHGAME():
    put("SEARCH GAME")
    Get_ok()
    start = get()
    if start != "START":
        Blad("Gdzie jest start?")


def WAIT():
    put("WAIT")
    Get_ok(2)

moves = ["ROCK", "PAPER", "SCISSORS"]

def MOVE():
    put(random.choice(moves))
    #put("PAPER")
    Get_ok()

def SCORE():
    put("SCORE")
    get()
    
def ROUND():
    put("ROUND")
    return int(get())

if __name__ == "__main__":
    LOGIN()
    JOINGAME("RPS")
    SEARCHGAME()
    round = 0
    while round < 5:
        round = ROUND()
        SCORE()
        MOVE()
        WAIT()
        
        
