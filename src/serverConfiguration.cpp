#include "serverConfiguration.h"
#include "LuaState.h"
#include "LuaGameHandler.h"

serverConfiguration &serverConfiguration::getConfiguration() {
    static serverConfiguration instance;

    return instance;
}

int serverConfiguration::loadConfiguration(std::string pathToConfigFile) {
    LuaState configFile;

    if(not configFile.load(pathToConfigFile)){
        return 1;
    }

    try{
        port = configFile.getGlobalInt("port");
        game_dir = configFile.getGlobalString("gameDir");
        games_files = configFile.getGlobalSeqListOfStringAsVector("gamesFiles");
        database_file = configFile.getGlobalString("userDatabaseFile");
        roundTimeInMilisec = configFile.getGlobalInt("roundTime");

    }catch (const GetGlobalError &error )
    {
        setDefaultConfiguration();
        return 2;
    }

    auto files = getFullPathToGamesFiles();
    bool result = true;

    for(auto file : files)
    {
        result &= checkGameFile(file);
    }

    return result ? 0 : 3;
}

void serverConfiguration::setDefaultConfiguration(){
    port = 1234;
    game_dir = "";
    games_files.clear();
    database_file = "database.txt";
    roundTimeInMilisec = 3000;
}

int serverConfiguration::getPort() const {
    return port;
}

void serverConfiguration::setPort(int _port) {
    serverConfiguration::port = _port;
}

const std::string &serverConfiguration::getGame_dir() const {
    return game_dir;
}

//void serverConfiguration::setGame_dir(const std::string &_game_dir) {
//    serverConfiguration::game_dir = _game_dir;
//}

const std::vector<std::string> &serverConfiguration::getGames_files() const {
    return games_files;
}

bool serverConfiguration::addGameToList(std::string pathToGame) {
    if(not checkGameFile(pathToGame)){
        return false;
    }

    serverConfiguration::games_files.push_back(pathToGame);
    return true;
}

void serverConfiguration::printConfiguration() {
    std::cout << "Port: " << port << std::endl;
    std::cout << "Database: " << database_file << std::endl;
    std::cout << "Games dir: " << game_dir << std::endl;
    std::cout << "Round time: " << roundTimeInMilisec << "ms" << std::endl;
    std::cout << "Games: " << std::endl;

    for(auto g : games_files)
    {
        std::cout << "\t* " << g << std::endl;
    }
}

std::vector<std::string> serverConfiguration::getFullPathToGamesFiles() {
    std::vector<std::string> res(games_files);

    for(auto &e : res){
        e = game_dir+e;
    }

    return res;
}

bool serverConfiguration::checkGameFile(std::string file) {
    LuaGameHandler lgh;
    bool result = lgh.load(file);
    if(not result){
        std::cout << "!! Server detected some problems with " << file << ":\n" << lgh.getLastLoadError() << std::endl;
    }
    return result;
}

const std::string &serverConfiguration::getDatabase_file() const {
    return database_file;
}

void serverConfiguration::setDatabase_file(const std::string &_database_file) {
    serverConfiguration::database_file = _database_file;
}

int serverConfiguration::getRoundTimeInMilisec() const {
    return roundTimeInMilisec;
}

void serverConfiguration::setRoundTimeInMilisec(int _roundTimeInMilisec) {
    serverConfiguration::roundTimeInMilisec = _roundTimeInMilisec;
}
