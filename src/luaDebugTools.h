#ifndef LUADEBUGTOOLS_H
#define LUADEBUGTOOLS_H

#include <iostream>
#include <lua.hpp>

static void stackDump (lua_State *L, std::string separator = "\n")
{
    int top = lua_gettop(L);

    std::cout << "Stack contains " << top << (top == 1 ? " element\n" : " elements.\n");

    for (int i = 1; i <= top; i++)
    {
        int t = lua_type(L, i);
        int number = i - top - 1;
        switch (t)
        {
            case LUA_TSTRING:
                std::cout << number << ":S: " << lua_tostring(L, i) << separator;
                break;

            case LUA_TBOOLEAN:
                std::cout << number << ":B: " << (lua_toboolean(L, i) ? "true" : "false") << separator;
                break;

            case LUA_TNUMBER:
                if (lua_isinteger(L, i))
                    std::cout << number << ":I: " << lua_tointeger(L, i) << separator;
                else
                    std::cout << number << ":D: " << lua_tonumber(L, i) << separator;
                break;

            default:
                std::cout << number << ": " << lua_typename(L, t) << " # " << lua_topointer(L, i) << separator;
                break;
        }
    }
    std::cout << std::endl;
}

void print_error(lua_State *L, const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);

    puts("");

    exit(-1);
}

#endif //LUADEBUGTOOLS_H
