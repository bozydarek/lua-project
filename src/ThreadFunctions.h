#ifndef THREADFUNCTIONS_H
#define THREADFUNCTIONS_H


#include "GameThreadHandler.h"
#include "ThreadSafeQueue.h"
#include <memory>
#include <chrono>
#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "posixwrapper.h"
#include <iostream>
#include <exception>
#include <algorithm>
using namespace std;

void loggerThreadFunction(std::shared_ptr<ThreadSafeQueue<string>> queue, bool & serverShouldEnd);

void gameThreadFunction(shared_ptr<GameThreadHandler> gameThreadHandler, bool & serverShouldEnd);

class GameThreadClass
{
private:
    map<int, shared_ptr<struct Client> > clients;   //client id, Client
    map<int, shared_ptr<LuaGameHandler> > games;    //roomNumber, Game

    shared_ptr<GameThreadHandler> gameThreadHandler;
    bool & serverShouldEnd;

    int minimumNumberOfPlayers;
    int maximumNumberOfPlayers;

    int currentGameRoomNumber = 0;

    string name;

    vector<shared_ptr<struct Client>> matchmaker;
    bool matchmakerReady = false;
    std::chrono::time_point<std::chrono::system_clock> matchmakerReadyTimePoint;

    void handleMessages();
    void handleMatchmaker();
    void handleNewRounds();
    void handleGamesEnd();

    void matchmakerCreateFullGameRooms();

    chrono::duration<double, std::milli> roundTime;
public:
    GameThreadClass(shared_ptr<GameThreadHandler> _gameThreadHandler, bool & _serverShouldEnd);

    void loop();

};


#endif // THREADFUNCTIONS_H
