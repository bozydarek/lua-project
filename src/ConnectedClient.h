#ifndef CONNECTEDCLIENT_H
#define CONNECTEDCLIENT_H

#include <string>
#include "usersdatabase.h"
#include "GameThreadHandler.h"
#include "posixwrapper.h"
#include <memory>
#include <iostream>
#include <map>

using namespace std;

enum class Status{
    LOGIN_USERNAME,
    LOGIN_PASSWORD,
    LOBBY,
    IN_GAME
};

class ConnectedClient
{
private:
    std::shared_ptr<UsersDatabase> usersDatabase;
    std::shared_ptr<GameThreadHandler> game;


public:
    ConnectedClient(std::shared_ptr<UsersDatabase> _usersDatabase);

    Status userStatus;
    int fileDescriptor;
    int userId;
    string userName;
    UserType userType;

    void handleRequest(string message, const map<string, shared_ptr<GameThreadHandler> > &games, bool &serverShouldEnd);

};

#endif // CONNECTEDCLIENT_H
