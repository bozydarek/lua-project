#ifndef THREADSAFEQUEUE_H
#define THREADSAFEQUEUE_H
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <iostream>
#include <memory>
#include <chrono>
using namespace std::chrono_literals;

using namespace std;

template <typename T> class ThreadSafeQueue
{
private:
    queue<T> que;

    mutex mut;
    condition_variable elementInQueue;


public:
    ThreadSafeQueue()
    {

    }

    int size()
    {
        std::unique_lock<std::mutex> lock(mut);

        return que.size();
    }

    bool empty()
    {
        std::unique_lock<std::mutex> lock(mut);
        return que.empty();
    }

    T pop(bool & serverShouldEnd)
    {
        std::unique_lock<std::mutex> lock(mut);
        while(que.empty())
        {
            if(elementInQueue.wait_for(lock,5000ms, [&]{return serverShouldEnd;}))    //lambda - predicate which returns ​false if the waiting should be continued.
            {
                throw runtime_error("SERVER END");
            }
        }

        T val = que.front();
        que.pop();
        return val;
    }

    T popWithTimeout()
    {
        std::unique_lock<std::mutex> lock(mut);
        if(que.empty())
        {
            if(elementInQueue.wait_for(lock,500ms) == cv_status::timeout)
            {
                throw runtime_error("TIMEOUT");
            }
        }

        T val = que.front();
        que.pop();
        return val;
    }

    void push(T element)
    {
        std::lock_guard<std::mutex> lock(mut);
        que.push(element);
        elementInQueue.notify_one();
    }
};

#endif // THREADSAFEQUEUE_H
