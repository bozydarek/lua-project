#ifndef User_H
#define User_H

#include <string>

enum class UserType{
    Admin,
    User,
    Observer
};

enum class UserStatus{
    loggedOut,
    connected
};

class User {
public:
    int id;
    std::string name;
    std::string password;
    UserType type;
    UserStatus status;
};


#endif // User_H
