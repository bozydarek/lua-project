#include "LuaGameHandler.h"

LuaGameHandler::LuaGameHandler() {

}

bool LuaGameHandler::loadGame(std::string pathToLuaFile){
    if(not state.load(pathToLuaFile)){
        lastError = "Cannot load file as Lua file. Check if file exist and is a proper Lua file.";
        return false;
    }

    bool result = true;
    std::vector<std::string> functionsToCheck = {"prepareGame","startGame","handlePlayerMove",
                                                 "playRound","getScore","notify","isGameOver",
                                                 "getView", "handleAdminFunctions"};
    for(auto func : functionsToCheck){
        result &= state.checkIfFunctionExist(func);
    }

    // get proper variables
    try {
        name = state.getGlobalString("gameName");
        shortName = state.getGlobalString("shortGameName");
        minimumNumberOfPlayers = state.getGlobalInt("minimumNumberOfPlayers");
        maximumNumberOfPlayers = state.getGlobalInt("maximumNumberOfPlayers");
    }
    catch(const GetGlobalError& e){
        lastError = "Problem with constants";
        return false;
    }

    if(minimumNumberOfPlayers > maximumNumberOfPlayers || minimumNumberOfPlayers < 0 || maximumNumberOfPlayers < 0) {
        lastError = "Logic error";
        return false;
    }

    if(shortName.length() < 1 || name.length() < 1) {
        lastError = "Logic error";
        return false;
    }

    if( not result ){
        lastError = "Problem with functions";
        return false;
    }

    return true;
}

bool LuaGameHandler::load(std::string pathToLuaFile) {

    if(not loadGame(pathToLuaFile)){
        return false;
    }

    auto testRes = testGame(pathToLuaFile);
    if( testRes != ""){
        lastError = testRes;
        return false;
    }

    return true;
}

const std::string &LuaGameHandler::getFullName() const {
    return name;
}

void LuaGameHandler::prepareGame(std::vector<int> &vector) {
    playersID = std::vector<int>(vector);
    state.callFunction("prepareGame", 1, 0, vector);
}

const std::string &LuaGameHandler::getShortName() const {
    return shortName;
}

const int &LuaGameHandler::getMinimumNumberOfPlayers() const
{
    return minimumNumberOfPlayers;
}

const int &LuaGameHandler::getMaximumNumberOfPlayers() const
{
    return maximumNumberOfPlayers;
}

const std::chrono::time_point<std::chrono::system_clock> &LuaGameHandler::getLastRoundStartTimePoint() const
{
    return lastRoundStartTimePoint;
}

const std::vector<int> LuaGameHandler::getPlayersIDVector() const
{
    return playersID;
}

void LuaGameHandler::startGame() {
    state.callFunction("startGame", 0, 0);
    lastRoundStartTimePoint = std::chrono::system_clock::now();
}

std::string LuaGameHandler::handlePlayerMove(int playerID, std::string move) {
    state.callFunction("handlePlayerMove", 2, 1, playerID, move);
    auto response = state.getStringFromStack();

    if (*(response.end()-1) != '\n')
        response += "\n";

    return response;
}

void LuaGameHandler::playRound() {
    state.callFunction("playRound", 0, 0);
    lastRoundStartTimePoint = std::chrono::system_clock::now();
}

std::map<int,int> LuaGameHandler::getScore() {
    state.callFunction("getScore", 0, 1);
    return state.getIntMapOfIntFromStack();
}

std::map<int, std::string> LuaGameHandler::getNotifyList() {
    state.callFunction("notify", 0, 1);
    return state.getIntMapOfStringFromStack();
}

bool LuaGameHandler::isGameOver() {
    state.callFunction("isGameOver", 0, 1);
    return state.getBoolFromStack();
}

const std::string &LuaGameHandler::getLastLoadError() const {
    return lastError;
}

std::string LuaGameHandler::getView() {
    state.callFunction("getView", 0, 1);
    return state.getStringFromStack();
}

std::string LuaGameHandler::handleAdminFunctions(std::string message) {
    state.callFunction("handleAdminFunctions", 1, 1, message);
    return state.getStringFromStack();
}

std::string LuaGameHandler::testGame(std::string pathToLuaFile) {
    std::string problems = "";
    LuaGameHandler testLG;
    testLG.loadGame(pathToLuaFile);

    try {
        auto someString = testLG.handlePlayerMove(0, "TEST");
    }
    catch(const std::exception& e){
        problems += "Problem with handlePlayerMove function\n";
    }

    try {
        auto someScore = testLG.getScore();
    }
    catch(const std::exception& e){
        problems += "Problem with getScore function\n";
    }

    try {
        auto someNotify = testLG.getNotifyList();
    }
    catch(const std::exception& e){
        problems += "Problem with getNotifyList function\n";
    }

    try {
        auto someBool = testLG.isGameOver();
    }
    catch(const std::exception& e){
        problems += "Problem with isGameOver function\n";
    }

    return problems;
}
