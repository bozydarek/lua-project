#ifndef LUASTATEEXCEPTIONS_H
#define LUASTATEEXCEPTIONS_H

#include <stdexcept>

class LuaStateExceptions : public std::runtime_error{
public:
    LuaStateExceptions(std::string errorMessage)
        : runtime_error("LuaState error: "+errorMessage) { }
};

class GetGlobalError : public LuaStateExceptions {
public:
    GetGlobalError(std::string additionalMessage = "")
        : LuaStateExceptions("getGlobal form Lua fail"+(additionalMessage==""?"":": "+additionalMessage)){  }
};

class CallFunctionError : public LuaStateExceptions {
public:
    CallFunctionError(std::string additionalMessage = "")
        : LuaStateExceptions("callFunction form Lua fail"+(additionalMessage==""?"":": "+additionalMessage)){  }
};


#endif //LUASTATEEXCEPTIONS_H
