#ifndef LUASTATE_H
#define LUASTATE_H

#include <lua.hpp>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include "LuaStateExceptions.h"

class LuaState {
    lua_State* state;
    std::string lastErrorMsg = "";
public:
    LuaState();
    ~LuaState();

    bool            load(std::string pathToLuaFile);
    std::string     getLastError();

    int             getGlobalInt(std::string varName)   throw(GetGlobalError);
    double          getGlobalDouble(std::string varName) throw(GetGlobalError);
    std::string     getGlobalString(std::string varName) throw(GetGlobalError);
    bool            getGlobalBool(std::string varName)  throw(GetGlobalError);

    int             getIntFromStack(int pos = -1) throw(GetGlobalError);
    double          getDoubleFromStack(int pos = -1) throw(GetGlobalError);
    std::string     getStringFromStack(int pos = -1) throw(GetGlobalError);
    bool            getBoolFromStack(int pos = -1) throw(GetGlobalError);

    std::vector<int>            getGlobalSeqListOfIntAsVector(std::string varName) throw(GetGlobalError);
    std::vector<double>         getGlobalSeqListOfDoubleAsVector(std::string varName) throw(GetGlobalError);
    std::vector<std::string>    getGlobalSeqListOfStringAsVector(std::string varName) throw(GetGlobalError);

    std::vector<int>            getListOfIntFromStackAsVector(int pos) throw(GetGlobalError);
    std::vector<double>         getListOfDoubleFromStackAsVector(int pos) throw(GetGlobalError);
    std::vector<std::string>    getListOfStringFromStackAsVector(int pos) throw(GetGlobalError);

    std::map<std::string, int>          getMapOfIntFromStack(int pos = -1) throw(GetGlobalError);
    std::map<std::string, double>       getMapOfDoubleFromStack(int pos = -1) throw(GetGlobalError);
    std::map<std::string, std::string>  getMapOfStringFromStack(int pos = -1) throw(GetGlobalError);

    std::map<std::string, int>          getGlobalMapOfInt(std::string varName) throw(GetGlobalError);
    std::map<std::string, double>       getGlobalMapOfDouble(std::string varName) throw(GetGlobalError);
    std::map<std::string, std::string>  getGlobalMapOfString(std::string varName) throw(GetGlobalError);

    std::map<int, int>          getIntMapOfIntFromStack(int pos = -1) throw(GetGlobalError);
    std::map<int, double>       getIntMapOfDoubleFromStack(int pos = -1) throw(GetGlobalError);
    std::map<int, std::string>  getIntMapOfStringFromStack(int pos = -1) throw(GetGlobalError);

    std::map<int, int>          getGlobalIntMapOfInt(std::string varName) throw(GetGlobalError);
    std::map<int, double>       getGlobalIntMapOfDouble(std::string varName) throw(GetGlobalError);
    std::map<int, std::string>  getGlobalIntMapOfString(std::string varName) throw(GetGlobalError);

    bool    checkIfFunctionExist(std::string funName);

    void    pushToStack(bool value);
    void    pushToStack(int value);
    void    pushToStack(double value);
    void    pushToStack(std::string value);
    void    pushToStack(const char* value);
    void    pushToStack();

    template<typename T>
    void    pushToStack(std::vector<T> &list) {
        int size = (int) list.size();
        lua_createtable(state, 0, size);

        for(int i=0; i<size; ++i){
            pushToStack(list[i]);
            lua_seti(state, -2, i+1);
        }
    }

    template<typename... Targs>
    void callFunction(std::string funName, int numberOfParameters, int numberOfArgsOnReturn, Targs... funArgs) throw(CallFunctionError, GetGlobalError)
    {
        lua_getglobal(state, funName.c_str());
        if (not lua_isfunction(state, -1))
        {
            lua_pop(state, 1);
            throw GetGlobalError();
        }

        prepareAndCallFunction(numberOfParameters, numberOfArgsOnReturn, numberOfParameters, funArgs...);
    }

#ifdef DEBUG
    void debug();
    lua_State*      getLuaState();
#endif

private:
    static int          _getIntFromStack(lua_State *state, int pos) throw(GetGlobalError);
    static double       _getDoubleFromStack(lua_State *state, int pos) throw(GetGlobalError);
    static std::string  _getStringFromStack(lua_State *state, int pos) throw(GetGlobalError);
    static bool         _getBoolFromStack(lua_State *state, int pos) throw(GetGlobalError);

    template <typename T, class F>
    std::vector<T> getGlobalSeqListAsVector(std::string varName, F getFun) throw(GetGlobalError);

    template <typename T, class F>
    std::vector<T> getSeqListAsVectorFromStack(int pos, F getFun) throw(GetGlobalError);

    template <typename T, class F>
    std::map<std::string, T> getGlobalMap(std::string varName, F getFun) throw(GetGlobalError);

    template <typename T, class F>
    std::map<std::string, T> getMapFromStack(int pos, F getFun) throw(GetGlobalError);

    template <typename T, class F>
    std::map<int, T> getGlobalIntMap(std::string varName, F getFun) throw(GetGlobalError);

    template <typename T, class F>
    std::map<int, T> getIntMapFromStack(int pos, F getFun) throw(GetGlobalError);

    template<typename T, typename... Targs>
    void prepareAndCallFunction(int numberOfParameters, int numberOfArgsOnReturn, int ammountOfParametersLeft, T value, Targs... funArgs) throw(CallFunctionError)
    {
        if(ammountOfParametersLeft == 0){
            throw CallFunctionError("Too many arguments (or not enough letters in format)");
        }
        pushToStack(value);
        prepareAndCallFunction(numberOfParameters, numberOfArgsOnReturn, ammountOfParametersLeft-1, funArgs...);
    }

    void prepareAndCallFunction(int numberOfParameters, int numberOfArgsOnReturn, int amountOfParametersLeft) throw(CallFunctionError){
        if(amountOfParametersLeft != 0){
            throw CallFunctionError("Too many letters in format (or not enough arguments)");
        }
        //debug();
        lua_pcall(state, numberOfParameters, numberOfArgsOnReturn, 0);
    }

};

#endif //LUASTATE_H
