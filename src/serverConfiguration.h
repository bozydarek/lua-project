#ifndef SERVERCONFIGURATION_H
#define SERVERCONFIGURATION_H

#include <string>
#include <vector>
#include <iostream>

class serverConfiguration {
    int port;
    std::string game_dir;
    std::vector<std::string> games_files;
    std::string database_file;

    int roundTimeInMilisec;
public:

public:
    static serverConfiguration &getConfiguration();

    serverConfiguration(serverConfiguration const &) = delete;
    void operator=(serverConfiguration const &)      = delete;

    int     loadConfiguration(std::string pathToConfigFile);
    void    setDefaultConfiguration();
    void    printConfiguration();

    int     getPort() const;
    void    setPort(int _port);

    int     getRoundTimeInMilisec() const;
    void    setRoundTimeInMilisec(int _roundTimeInMilisec);

    const   std::string &getGame_dir() const;
//    void    setGame_dir(const std::string &_game_dir);

    const   std::string &getDatabase_file() const;
    void    setDatabase_file(const std::string &_database_file);

    const   std::vector<std::string> &getGames_files() const;
    bool    addGameToList(std::string pathToGame);
    std::vector<std::string> getFullPathToGamesFiles();

private:
    bool checkGameFile(std::string file);

    serverConfiguration() { setDefaultConfiguration(); }
};


#endif // SERVERCONFIGURATION_H
