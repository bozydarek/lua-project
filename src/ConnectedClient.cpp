#include "ConnectedClient.h"

ConnectedClient::ConnectedClient(std::shared_ptr<UsersDatabase> _usersDatabase) : usersDatabase{_usersDatabase}
{
    userStatus = Status::LOGIN_USERNAME;

}

// This function need some good refactor
void ConnectedClient::handleRequest(string message, const map<string, shared_ptr<GameThreadHandler> > &games, bool & serverShouldEnd)
{
    if(message.length() < 1){
        return;
    }

    switch (userStatus)
    {
    case Status::LOGIN_USERNAME:
    {
        string msg = "PASSWORD\n";
        Writen(fileDescriptor, msg);
        userStatus = Status::LOGIN_PASSWORD;
        userName = message;
        break;
    }
    case Status::LOGIN_PASSWORD:
    {
        if(usersDatabase->userExists(userName))
        {
            if(usersDatabase->passwordCorrect(userName, message))
            {
                if(usersDatabase->getUserStatus(userName) == UserStatus::loggedOut)
                {
                    string msg = "OK\n";
                    Writen(fileDescriptor, msg);
                    userStatus = Status::LOBBY;
                    userType = usersDatabase->getUserType(userName);
                    usersDatabase->storeUserStatus(userName, UserStatus::connected);
                    userId = usersDatabase->getUserId(userName);
                }

            }

        }
        if(userStatus != Status::LOBBY)
        {
            userStatus = Status::LOGIN_USERNAME;
            userName = "";
            string msg = "LOGIN\n";
            Writen(fileDescriptor, msg);
        }
        break;
    }
    case Status::LOBBY:
    {
        if(message == string("SHUTDOWN"))
        {
            std::string msg;
            if(userType == UserType::Admin)
            {
                serverShouldEnd = true;
                msg = "OK\n";

            }
            else
            {
                msg = "YOU ARE NOT ALLOWED TO DO THIS\n";
            }
            Writen(fileDescriptor, msg);
        }
        else if(message == string("SHOW CLIENTS"))
        {
            std::string msg;
            if(userType == UserType::Admin)
            {
                auto connectedUsers = usersDatabase->getConnectedUsers();
                std::string list = "List of "+to_string(connectedUsers.size())+" connected users:\n";

                for (auto user : connectedUsers){
                    list += "* "+ to_string(user->id) +":"+ user->name + "\n";
                }

                Writen(fileDescriptor, (void *) list.c_str(), list.size());

            }
            else
            {
                msg = "YOU ARE NOT ALLOWED TO DO THIS\n";
                Writen(fileDescriptor, msg);
            }
        }
        else if(message == string("LIST GAMES")){
            std::string output = "List of " + to_string(games.size()) + " available games:\n";
            for (auto g : games){
                output += "* "+ g.second->getShortName() + " ~ " + g.second->getFullName()+"\n";
            }

            Writen(fileDescriptor, output);
        }
        else if(message == string("HELP")) {
            std::string output = "List of available commands:\n";

            output += "* LIST GAMES - show list of available games\n";
            output += "* JOIN GAME *short name* - join to specific game lobby\n";
            output += "* SEARCH GAME - join matchmaker (while in game lobby) \n";

            if(userType == UserType::Admin)
            {
                output += "* SHUTDOWN - shutdown the server\n";
                output += "* SHOW CLIENTS - show list of connected clients\n";
            }

            Writen(fileDescriptor, output);
        }
        else if(message.length() > 10)
        {
            std::string msg;
            if(message.substr(0, 10) == string("JOIN GAME "))
            {
                string gameName = message.substr(10, message.length());

                if(games.end() == games.find(gameName))
                {
                    msg = "GAME NOT FOUND\n";
                }
                else
                {
                    game = games.find(gameName)->second;
                    msg = "OK\n";
                    userStatus = Status::IN_GAME;
                    game->sendMessage(userId, fileDescriptor, string("JOINED TO GAME"));
                }

                Writen(fileDescriptor, msg);
            }
        }
        else
        {
            std::string msg = "UNKNOWN COMMAND\n";
            Writen(fileDescriptor, msg);
        }

        break;
    }
    case Status::IN_GAME:
    {
#ifdef DEBUG
        std::cout << "RECEIVE Message form user " << userId << " : #" << message << "#" << std::endl;
#endif
        game->sendMessage(userId, fileDescriptor, message);
        break;
    }
    default:
    {
        break;
    }
    }
}
