#include "ThreadFunctions.h"
#include "serverConfiguration.h"

void loggerThreadFunction(std::shared_ptr<ThreadSafeQueue<string> > queue, bool & serverShouldEnd)
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    string filename = string("logs/");

    stringstream ss;
    ss << put_time(localtime(&now_c), "%F-%H-%M-%S");
    filename += ss.str();
    filename += string(".log");
    fstream file;
    file.open(filename, ios::out | std::fstream::app);

    if(!file.good())
    {
        err_sys("Could not create log file");
    }

    while(!serverShouldEnd)
    {
        try
        {
            string log_message;
            log_message = queue->pop(serverShouldEnd);
            file<<log_message<<endl;
        }
        catch(exception & e)
        {

        }

    }
    file.close();
}

enum class Status{
    LOBBY,
    MATCHMAKER,
    GAME
};

struct Client
{
    int clientId;
    int clientFD;
    bool isConnectedToServer;
    Status status;
    shared_ptr <LuaGameHandler> gameRoom;
};


void gameThreadFunction(shared_ptr<GameThreadHandler> gameThreadHandler, bool &serverShouldEnd)
{
    GameThreadClass game(gameThreadHandler, serverShouldEnd);

    game.loop();
}

void GameThreadClass::handleMessages()
{
    try
    {
        tuple<int, int, string> tmp;
        tmp = gameThreadHandler->queue.popWithTimeout();

        int clientId = get<0>(tmp);
        int clientFD = get<1>(tmp);
        string &message = get<2>(tmp);

        if(message == string("USER DISCONNECTED"))
        {
            clients[clientId]->isConnectedToServer = false;

            //if client is in game then it will be removed from lobby when game ends
            if(clients[clientId]->status != Status::GAME)
            {
                if(clients[clientId]->status == Status::MATCHMAKER)
                {
                    matchmaker.erase(
                                remove_if(matchmaker.begin(),
                                            matchmaker.end(),
                                            [&] (shared_ptr<struct Client> tmpClient) {return (tmpClient->clientId==clientId);}),
                                matchmaker.end());

                }

                clients.erase(clientId);
            }
        }
        else if(message == string("JOINED TO GAME"))
        {
            std::shared_ptr<struct Client> newClient = std::make_shared<struct Client>();
            newClient->clientFD = clientFD;
            newClient->clientId = clientId;
            newClient->status = Status::LOBBY;
            newClient->isConnectedToServer = true;
            clients.insert(pair<int, shared_ptr<struct Client>>(newClient->clientId, newClient) );
        }
        else if(message == "SEARCH GAME")
        {
            shared_ptr<struct Client> client = clients[clientId];
            client->isConnectedToServer = true;
            if(client->status == Status::LOBBY)
            {
                client->status = Status::MATCHMAKER;
                matchmaker.push_back(client);
                Writen(client->clientFD, "OK\n");
            }
            else
            {
                Writen(client->clientFD, "ERROR 100\n"); //client already in game or in matchmaker

            }
        }
        else
        {
            shared_ptr<struct Client> client = clients[clientId];
            client->isConnectedToServer = true;
            if(client->status != Status::GAME)
            {
                cout<<"Client "<<clientId<<" passed unknown command: "<<message<<endl;
                Writen(client->clientFD, "COMMAND NOT FOUND\n");
            }
            else
            {
#ifdef DEBUG
                std::cout << "PASS to LUA msg form user " << clientId << " : #" <<message<< "#" << std::endl;
#endif
                string response = client->gameRoom->handlePlayerMove(clientId, message);
                Writen(clientFD, response);
            }
        }
    }
    catch(exception & e)
    {
    }
}

void GameThreadClass::handleMatchmaker()
{
    matchmakerCreateFullGameRooms();

    if(matchmaker.size() < minimumNumberOfPlayers)
    {
        matchmakerReady = false;
    }

    if(!matchmakerReady && matchmaker.size()>=minimumNumberOfPlayers)
    {
        matchmakerReadyTimePoint = chrono::system_clock::now();
        matchmakerReady = true;
    }

    if(matchmakerReady)
    {
        chrono::duration<double> elapsedSeconds = chrono::system_clock::now() - matchmakerReadyTimePoint;
        if(elapsedSeconds > 5s)
        {
            matchmakerReady = false;

            vector<int> playersId;
            shared_ptr<LuaGameHandler> newGame = std::make_shared<LuaGameHandler>();
            newGame->load(gameThreadHandler->getFileNamePath());

            for(auto tmpClient : matchmaker)
            {
                playersId.push_back(tmpClient->clientId);
                tmpClient->gameRoom = newGame;
                tmpClient->status = Status::GAME;
            }

            games.insert(pair<int, shared_ptr<LuaGameHandler>>(currentGameRoomNumber++, newGame));

            newGame->prepareGame(playersId);
            newGame->startGame();

            for(auto tmpClient : matchmaker)
            {
                Writen(tmpClient->clientFD, "START\n");
            }
            matchmaker.clear();
        }
    }
}

void GameThreadClass::handleNewRounds()
{
    for(auto pairNumberGame : games)
    {
        shared_ptr<LuaGameHandler> game = pairNumberGame.second;
        chrono::duration<double, std::milli> elapsedSeconds = chrono::system_clock::now() - game->getLastRoundStartTimePoint();
        if(elapsedSeconds > roundTime)
        {
            game->playRound();

            map<int, string> notifyList = game->getNotifyList();

            vector<int> players = game->getPlayersIDVector();

            for(pair<const int, string> notify : notifyList)
            {
                if(find(players.begin(),players.end(),notify.first) != players.end())
                {
                    if(clients[notify.first]->isConnectedToServer)
                    {
                        if( *(notify.second.end()-1) != '\n')
                            notify.second += "\n";

                        Writen(clients[notify.first]->clientFD, notify.second);
                    }
                }
            }

        }
    }

}

void GameThreadClass::handleGamesEnd()
{
    vector<int> closedGameRooms;
    for(auto pairNumberGame : games)
    {
        shared_ptr<LuaGameHandler> game = pairNumberGame.second;
        int roomNumber = pairNumberGame.first;
        if(game->isGameOver())
        {
            map<int,int> score = game->getScore();
            vector<int> players = game->getPlayersIDVector();

            cout<<"Game "<<name<<", room: "<<roomNumber<<" finished, score:"<<endl;
            for(auto tmpPair : score)
            {
                if(find(players.begin(),players.end(),tmpPair.first) != players.end())
                {
                    cout<<"Client id: "<<tmpPair.first<<", score: "<<tmpPair.second<<endl;
                }
            }

            for(int clientID : game->getPlayersIDVector())
            {
                if(clients[clientID]->isConnectedToServer)
                {
                    clients[clientID]->gameRoom.reset();
                    clients[clientID]->status = Status::LOBBY;
                    Writen(clients[clientID]->clientFD, "GAME OVER\n");
                }
                else
                {
                    clients.erase(clientID);
                }
            }

            closedGameRooms.push_back(roomNumber);
        }
    }

    for(auto roomNumber : closedGameRooms)
    {
        games.erase(roomNumber);
    }
}

void GameThreadClass::matchmakerCreateFullGameRooms()
{
    while(matchmaker.size() >= maximumNumberOfPlayers)
    {
        shared_ptr<LuaGameHandler> newGame = std::make_shared<LuaGameHandler>();
        newGame->load(gameThreadHandler->getFileNamePath());

        vector<int> playersId;

        vector<shared_ptr<struct Client> > players(matchmaker.begin(), matchmaker.begin()+maximumNumberOfPlayers);
        matchmaker.erase(matchmaker.begin(), matchmaker.begin()+maximumNumberOfPlayers);

        for(auto tmpClient : players)
        {
            playersId.push_back(tmpClient->clientId);
            tmpClient->gameRoom = newGame;
            tmpClient->status = Status::GAME;
        }
        games.insert(pair<int, shared_ptr<LuaGameHandler>>(currentGameRoomNumber++, newGame));

        newGame->prepareGame(playersId);
        newGame->startGame();

        for(auto tmpClient : players)
        {
            Writen(tmpClient->clientFD, "START\n");
        }
    }
}

GameThreadClass::GameThreadClass(shared_ptr<GameThreadHandler> _gameThreadHandler, bool &_serverShouldEnd)
    : gameThreadHandler{_gameThreadHandler},
      serverShouldEnd{_serverShouldEnd}
{
    LuaGameHandler tmpGameHandler;
    tmpGameHandler.load(_gameThreadHandler->getFileNamePath());
    minimumNumberOfPlayers = tmpGameHandler.getMinimumNumberOfPlayers();
    maximumNumberOfPlayers = tmpGameHandler.getMaximumNumberOfPlayers();

    name = tmpGameHandler.getFullName();

    auto time = serverConfiguration::getConfiguration().getRoundTimeInMilisec();
    roundTime = chrono::duration<double, std::milli>(time);
}

void GameThreadClass::loop()
{
    while(not serverShouldEnd)
    {
        handleMessages();

        handleMatchmaker();

        handleNewRounds();

        handleGamesEnd();


    }
}
