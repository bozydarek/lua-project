#ifndef LOGGER_H
#define LOGGER_H

#include "ThreadSafeQueue.h"
#include <string>
#include <chrono>
#include <memory>
#include <sstream>
#include <future>
#include <iomanip>
#include <iostream>


class Logger
{
private:
    std::shared_ptr<ThreadSafeQueue<string>> queue;
public:
    Logger(std::shared_ptr<ThreadSafeQueue<string>> _queue);
    void write(string msg);
};

#endif // LOGGER_H
