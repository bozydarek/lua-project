#include <assert.h>
#include "GameThreadHandler.h"

GameThreadHandler::GameThreadHandler(string _fullName,
                                     string _shortName,
                                     string _fileNamePath) : fullName{_fullName}, shortName{_shortName}, fileNamePath{_fileNamePath}
{

}

void GameThreadHandler::sendMessage(int _clientId, int _clientFD, string _message)
{
    queue.push(make_tuple(_clientId, _clientFD, _message));
}

const string &GameThreadHandler::getFileNamePath() const
{
    return fileNamePath;
}

const string &GameThreadHandler::getFullName() const {
    return fullName;
}

const string &GameThreadHandler::getShortName() const {
    return shortName;
}
