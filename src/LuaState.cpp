#include "LuaState.h"
#include "luaDebugTools.h"

#include <unistd.h>
#include <map>

LuaState::LuaState()
{
    state = luaL_newstate();
    luaL_openlibs(state);
}

LuaState::~LuaState() {
    lua_close(state);
}

bool LuaState::load(std::string pathToLuaFile) {
    auto len = pathToLuaFile.length();

    if( access(pathToLuaFile.c_str(), F_OK) == -1 )
    {
        lastErrorMsg = pathToLuaFile + " doesn't exist";
        return false;
    }

    if (len < 4 || pathToLuaFile.substr(len - 4) != ".lua")
    {
        lastErrorMsg = pathToLuaFile + " is not lua file";
        return false;
    }

    if (luaL_loadfile(state, pathToLuaFile.c_str())  || lua_pcall(state, 0, 0, 0)) {
        lastErrorMsg = "Cannot load " + pathToLuaFile;
        return false;
    }

    return true;
}

std::string LuaState::getLastError() {
    return lastErrorMsg;
}

int LuaState::_getIntFromStack(lua_State *state, int pos) throw(GetGlobalError) {
    int isNum, result;

    result = (int)lua_tointegerx(state, pos, &isNum);
    lua_pop(state, 1);

    if (!isNum)
    {
        throw GetGlobalError();
    }

    return result;
}

double LuaState::_getDoubleFromStack(lua_State *state, int pos) throw(GetGlobalError) {
    int isNum;
    double result = (double)lua_tonumberx(state, pos, &isNum);
    lua_pop(state, 1);

    if (!isNum)
    {
        throw GetGlobalError();
    }

    return result;
}

std::string LuaState::_getStringFromStack(lua_State *state, int pos) throw(GetGlobalError) {
    if (lua_type(state, pos) != LUA_TSTRING)
    {
        lua_pop(state, 1);
        throw GetGlobalError();
    }

    std::string result (lua_tostring(state, pos));
    lua_pop(state, 1);

    return result;
}

bool LuaState::_getBoolFromStack(lua_State *state, int pos) throw(GetGlobalError) {
    if (lua_type(state, pos) != LUA_TBOOLEAN)
    {
        lua_pop(state, 1);
        throw GetGlobalError();
    }

    bool result = (bool) lua_toboolean(state, pos);
    lua_pop(state, 1);

    return result;
}

int LuaState::getGlobalInt(std::string varName) throw(GetGlobalError) {
    lua_getglobal(state, varName.c_str());
    return _getIntFromStack(state, -1);
}

double LuaState::getGlobalDouble(std::string varName) throw(GetGlobalError) {
    lua_getglobal(state, varName.c_str());
    return _getDoubleFromStack(state, -1);
}

std::string LuaState::getGlobalString(std::string varName) throw(GetGlobalError) {
    lua_getglobal(state, varName.c_str());
    return _getStringFromStack(state, -1);
}

bool LuaState::getGlobalBool(std::string varName) throw(GetGlobalError) {
    lua_getglobal(state, varName.c_str());
    return _getBoolFromStack(state, -1);
}

template <typename T, class F>
std::vector<T> LuaState::getGlobalSeqListAsVector(std::string varName, F getFun) throw(GetGlobalError) {
    std::vector<T> res;

    try{
        lua_getglobal(state, varName.c_str());
        res = getSeqListAsVectorFromStack<T>(-1, getFun);
    }catch (const GetGlobalError &e){
        throw GetGlobalError(varName + " is not a lua table");
    }

    return res;
}

template <typename T, class F>
std::vector<T> LuaState::getSeqListAsVectorFromStack(int pos, F getFun) throw(GetGlobalError) {
    if (lua_type(state, pos) != LUA_TTABLE)
    {
        lua_pop(state, 1);
        throw GetGlobalError();
    }

    unsigned long len = (unsigned long)luaL_len(state, pos);

    std::vector<T> list;
    list.reserve(len);

    for (unsigned long i = 0; i < len; ++i) {
        lua_geti(state, -1, (lua_Integer) (i + 1));
        T val = getFun(state, -1);
        list.push_back(val);
    }

    return list;
}

template <typename T, class F>
std::map<std::string, T> LuaState::getGlobalMap(std::string varName, F getFun) throw(GetGlobalError){
    std::map<std::string, T> res;

    try{
        lua_getglobal(state, varName.c_str());
        res = getMapFromStack<T>(-1, getFun);
    }catch (const GetGlobalError &e){
        throw GetGlobalError(varName + " is not a lua table");
    }

    return res;
}

template <typename T, class F>
std::map<std::string, T> LuaState::getMapFromStack(int pos, F getFun) throw(GetGlobalError){
    if (lua_type(state, pos) != LUA_TTABLE)
    {
        lua_pop(state, 1);
        throw GetGlobalError();
    }

    std::map<std::string, T> res;

    pushToStack();
    while (lua_next(state ,-2) != 0) {
        if(lua_type(state,-2) == LUA_TSTRING){
            std::string name = lua_tostring(state, -2);
            res[name] = getFun(state, -1);
        }
        else{
            lua_pop(state,1);
        }
    }
    lua_pop(state,1);

    return res;
}

template <typename T, class F>
std::map<int, T> LuaState::getGlobalIntMap(std::string varName, F getFun) throw(GetGlobalError){
    std::map<int, T> res;

    try{
        lua_getglobal(state, varName.c_str());
        res = getIntMapFromStack<T>(-1, getFun);
    }catch (const GetGlobalError &e){
        throw GetGlobalError(varName + " is not a lua table");
    }

    return res;
}

template <typename T, class F>
std::map<int, T> LuaState::getIntMapFromStack(int pos, F getFun) throw(GetGlobalError){
    if (lua_type(state, pos) != LUA_TTABLE)
    {
        lua_pop(state, 1);
        throw GetGlobalError();
    }

    std::map<int, T> res;

    pushToStack();
    while (lua_next(state ,-2) != 0) {
        if(lua_type(state,-2) == LUA_TNUMBER){
            int name = (int) lua_tonumber(state, -2);
            res[name] = getFun(state, -1);
        }
        else{
            lua_pop(state,1);
        }
    }
    lua_pop(state,1);

    return res;
}

std::vector<std::string> LuaState::getGlobalSeqListOfStringAsVector(std::string varName) throw(GetGlobalError){
    return getGlobalSeqListAsVector<std::string>(varName, _getStringFromStack);
}

std::vector<int> LuaState::getGlobalSeqListOfIntAsVector(std::string varName) throw(GetGlobalError){
    return getGlobalSeqListAsVector<int>(varName, _getIntFromStack);
}

std::vector<double> LuaState::getGlobalSeqListOfDoubleAsVector(std::string varName) throw(GetGlobalError){
    return getGlobalSeqListAsVector<double>(varName, _getDoubleFromStack);
}

std::vector<std::string> LuaState::getListOfStringFromStackAsVector(int pos) throw(GetGlobalError){
    return getSeqListAsVectorFromStack<std::string>(pos, _getStringFromStack);
}

std::vector<int> LuaState::getListOfIntFromStackAsVector(int pos) throw(GetGlobalError){
    return getSeqListAsVectorFromStack<int>(pos, _getIntFromStack);
}

std::vector<double> LuaState::getListOfDoubleFromStackAsVector(int pos) throw(GetGlobalError){
    return getSeqListAsVectorFromStack<double>(pos, _getDoubleFromStack);
}

#ifdef DEBUG
void LuaState::debug() {
    stackDump(state);
}

lua_State *LuaState::getLuaState() {
    return state;
}
#endif

void LuaState::pushToStack(bool value){
    lua_pushboolean(state, value);
}

void LuaState::pushToStack(int value){
    lua_pushinteger(state, value);
}

void LuaState::pushToStack(double value){
    lua_pushnumber(state, value);
}

void LuaState::pushToStack(std::string value){
    lua_pushstring(state, value.c_str());
}

void LuaState::pushToStack(const char* value){
    lua_pushstring(state, value);
}

void LuaState::pushToStack(){
    lua_pushnil(state);
}

int LuaState::getIntFromStack(int pos) throw(GetGlobalError) {
    return _getIntFromStack(state, pos);
}

double LuaState::getDoubleFromStack(int pos) throw(GetGlobalError) {
    return _getDoubleFromStack(state, pos);
}

std::string LuaState::getStringFromStack(int pos) throw(GetGlobalError) {
    return _getStringFromStack(state, pos);
}

bool LuaState::getBoolFromStack(int pos) throw(GetGlobalError) {
    return _getBoolFromStack(state, pos);
}

bool LuaState::checkIfFunctionExist(std::string funName) {
    lua_getglobal(state, funName.c_str());
    auto res = lua_isfunction(state, -1);
    lua_pop(state, 1);

    return res;
}

std::map<std::string, int> LuaState::getMapOfIntFromStack(int pos) throw(GetGlobalError) {
    return getMapFromStack<int>(pos, _getIntFromStack);
}

std::map<std::string, double> LuaState::getMapOfDoubleFromStack(int pos) throw(GetGlobalError) {
    return getMapFromStack<double>(pos, _getDoubleFromStack);
}

std::map<std::string, std::string> LuaState::getMapOfStringFromStack(int pos) throw(GetGlobalError) {
    return getMapFromStack<std::string>(pos, _getStringFromStack);
}

std::map<std::string, int> LuaState::getGlobalMapOfInt(std::string varName) throw(GetGlobalError) {
    return getGlobalMap<int>(varName, _getIntFromStack);
}

std::map<std::string, double> LuaState::getGlobalMapOfDouble(std::string varName) throw(GetGlobalError) {
    return getGlobalMap<double>(varName, _getDoubleFromStack);
}

std::map<std::string, std::string> LuaState::getGlobalMapOfString(std::string varName) throw(GetGlobalError) {
    return getGlobalMap<std::string>(varName, _getStringFromStack);
}


std::map<int, int> LuaState::getIntMapOfIntFromStack(int pos) throw(GetGlobalError) {
    return getIntMapFromStack<int>(pos, _getIntFromStack);
}

std::map<int, double> LuaState::getIntMapOfDoubleFromStack(int pos) throw(GetGlobalError) {
    return getIntMapFromStack<double>(pos, _getDoubleFromStack);
}

std::map<int, std::string> LuaState::getIntMapOfStringFromStack(int pos) throw(GetGlobalError) {
    return getIntMapFromStack<std::string>(pos, _getStringFromStack);
}

std::map<int, int> LuaState::getGlobalIntMapOfInt(std::string varName) throw(GetGlobalError) {
    return getGlobalIntMap<int>(varName, _getIntFromStack);
}

std::map<int, double> LuaState::getGlobalIntMapOfDouble(std::string varName) throw(GetGlobalError) {
    return getGlobalIntMap<double>(varName, _getDoubleFromStack);
}

std::map<int, std::string> LuaState::getGlobalIntMapOfString(std::string varName) throw(GetGlobalError) {
    return getGlobalIntMap<std::string>(varName, _getStringFromStack);
}
