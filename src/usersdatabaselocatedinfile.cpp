#include "usersdatabaselocatedinfile.h"

UsersDatabaseLocatedInFile::UsersDatabaseLocatedInFile(string _fileName) : fileName{_fileName}
{
    int id;
    string name;
    string password;
    int type;
    ifstream fileStream(fileName);
    while(fileStream.good())
    {
        fileStream>>id>>name>>password>>type;
        User user;
        user.id = id;
        user.name = name;
        user.password = password;
        user.type = (UserType)type;
        user.status = UserStatus::loggedOut;
        users.push_back(user);
    }
}

UsersDatabaseLocatedInFile::~UsersDatabaseLocatedInFile()
{

}

bool UsersDatabaseLocatedInFile::userExists(string userName)
{
    for (const User &user : users)
    {
        if(user.name == userName)
        {
            return true;
        }
    }
    return false;
}

bool UsersDatabaseLocatedInFile::passwordCorrect(string userName, string userPassword)
{
    auto user = getUserByName(userName);

    if(user.name == userName && user.password == userPassword)
    {
        return true;
    }
    return false;
}

int UsersDatabaseLocatedInFile::getUserId(string userName)
{
    return getUserByName(userName).id;
}

UserType UsersDatabaseLocatedInFile::getUserType(string userName)
{
    return getUserByName(userName).type;
}

User& UsersDatabaseLocatedInFile::getUserByName(string userName)
{
    for (auto &user : users)
    {
        if(user.name == userName)
        {
            return user;
        }
    }

    throw runtime_error("User not found");
}

User &UsersDatabaseLocatedInFile::getUserByID(int userID) {
    for (auto &user : users)
    {
        if(user.id == userID)
        {
            return user;
        }
    }

    throw runtime_error("User not found");
}


unsigned long UsersDatabaseLocatedInFile::getNumberOfUsers() {
    return users.size();
}

std::vector<std::shared_ptr<User>> UsersDatabaseLocatedInFile::getConnectedUsers() {
    std::vector<std::shared_ptr<User>> res;

    for( auto user : users ){
        if( user.status == UserStatus::connected ){
            res.emplace_back(std::make_shared<User>(user));
        }
    }

    return res;
}

void UsersDatabaseLocatedInFile::storeUserStatus(std::string userName, UserStatus newStatus) {
    getUserByName(userName).status = newStatus;
}

UserStatus UsersDatabaseLocatedInFile::getUserStatus(std::string userName) {
    return getUserByName(userName).status;
}

std::string UsersDatabaseLocatedInFile::getUserName(int userID) {
    return getUserByID(userID).name;
}

UserType UsersDatabaseLocatedInFile::getUserType(int userID) {
    return getUserByID(userID).type;
}
