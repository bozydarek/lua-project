#include "Server.h"
#include <sstream>

void Server::checkForNewClientConnection()
{
    if (FD_ISSET(listenfd, &rset)) {	/* new client connection */
        clilen = sizeof(cliaddr);
        connfd = Accept(listenfd, (SA *) &cliaddr, &clilen);
        char ip[15];

        ostringstream osstream;
        osstream <<"new client: "<<Inet_ntop(AF_INET, &cliaddr.sin_addr, ip, 15)<<" port "<<ntohs(cliaddr.sin_port)<<" fd: "<<connfd;

        logger->write(osstream.str());
        cout << osstream.str() << endl;

        int i;
        for (i = 0; i < FD_SETSIZE; i++)
            if (client[i] < 0) {
                client[i] = connfd;	/* save descriptor */
                break;
            }
        if (i == FD_SETSIZE)
            err_sys("too many clients");

        FD_SET(connfd, &allset);	/* add new descriptor to set */

        ConnectedClient connClient(usersDatabase);
        connClient.fileDescriptor = connfd;
        clients.insert(pair<int, ConnectedClient>(connfd, connClient));

        string msg = "LOGIN\n";
        Writen(connfd, msg);


        if (connfd > maxfd)
            maxfd = connfd;			/* for select */
        if (i > maxi)
            maxi = i;				/* max index in client[] array */

        if (--nready <= 0)
            readableDescriptors = false;				/* no more readable descriptors */
    }

}

void Server::checkAllClientsForData()
{
    for (int i = 0; i <= maxi; i++) {	/* check all clients for data */
        if ( (sockfd = client[i]) < 0)
            continue;
        if (FD_ISSET(sockfd, &rset))
        {
            if ( (n = Read(sockfd, buf, MAXLINE)) == 0) {
                /*4connection closed by client */
                auto c = clients.find(sockfd)->second;
                try {
                    usersDatabase->storeUserStatus(c.userName, UserStatus::loggedOut);
                }catch(const std::exception &e){
//                    logger->write("User"+c.userName+" not found in database");
                    logger->write(e.what());
                }

                c.handleRequest(string("USER DISCONNECTED"), games, serverShouldEnd);
                clients.erase(sockfd);
                Close(sockfd);
                FD_CLR(sockfd, &allset);
                client[i] = -1;
            }
            else
            {
                string msg(buf, (size_t) n);
                if(msg[msg.length()-1] == '\n')
                {
                    msg.erase(msg.length()-1, 1);
                }
                clients.find(sockfd)->second.handleRequest(msg, games, serverShouldEnd);
            }

            if (--nready <= 0)
                break;				/* no more readable descriptors */
        }
    }
}

void Server::createGameThreadHandlers(vector<string> gamesPaths)
{
    for(string path : gamesPaths)
    {
        LuaGameHandler tmpLuaGameHandler;
        tmpLuaGameHandler.load(path);
        string fullName = tmpLuaGameHandler.getFullName();
        string shortName = tmpLuaGameHandler.getShortName();

        shared_ptr<GameThreadHandler> tmpGameThreadHandler = make_shared<GameThreadHandler>(fullName, shortName, path);

        games.insert(pair<string,shared_ptr<GameThreadHandler> > (shortName, tmpGameThreadHandler));

//        thread t(gameThreadFunction, tmpGameThreadHandler);
        gameThreads.emplace_back(gameThreadFunction, tmpGameThreadHandler, ref(serverShouldEnd));
    }
}

Server::Server(std::shared_ptr<UsersDatabase> _usersDatabase,
               uint16_t serverPort,
               vector<string> gamesPaths,
               std::shared_ptr<ThreadSafeQueue<string>> _loggerQueue,
               bool &_serverShouldEnd) : usersDatabase{_usersDatabase}, serverShouldEnd(_serverShouldEnd)

{
    logger = std::unique_ptr<Logger>(new Logger(_loggerQueue));


    listenfd = Socket(AF_INET, SOCK_STREAM, 0);
    int enable = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,&enable, sizeof(int));

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(serverPort);

    Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

    Listen(listenfd, LISTENQ);

    maxfd = listenfd;			/* initialize */
    maxi = -1;					/* index into client[] array */
    for (int i = 0; i < FD_SETSIZE; i++)
        client[i] = -1;			/* -1 indicates available entry */
    FD_ZERO(&allset);
    FD_SET(listenfd, &allset);

    string logMsg = "Started server on port " + std::to_string(serverPort);
    logger->write(logMsg);

    createGameThreadHandlers(gamesPaths);

}

Server::~Server()
{
    for(thread & t : gameThreads)
    {
        t.join();
    }
}

void Server::loop()
{
    while (!serverShouldEnd) {
        rset = allset;		/* structure assignment */
        nready = Select(maxfd+1, &rset, NULL, NULL, NULL);
        readableDescriptors = true;

        checkForNewClientConnection();

        if(readableDescriptors)
        {
            checkAllClientsForData();
        }


    }
}
