#include "posixwrapper.h"

int fdIsValid(int fd)
{
    return fcntl(fd, F_GETFD) != -1 || errno != EBADF;
}

void err_sys(string funcName)
{
    std::cout << funcName << "(...) failed. \033[1;31mError:\033[0m " << errno << " - "<< strerror (errno) << "\n";
    exit(1);
}

void Close(int fd)
{
    if (close(fd) == -1)
        err_sys("close");
}

int Socket(int family, int type, int protocol)
{
    int	fd = socket (family, type, protocol);
    if( fd == -1 )
        err_sys("socket");

    return fd;
}

/* Write "n" bytes to a descriptor. */
ssize_t writen(int fd, const void *vptr, size_t n)
{
    size_t		nleft;
    ssize_t		nwritten;
    const char	*ptr;

    ptr = (const char*) vptr;
    nleft = n;
    while (nleft > 0) {
        if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
            if (nwritten < 0 && errno == EINTR)
                nwritten = 0;		/* and call write() again */
            else
                return(-1);			/* error */
        }

        nleft -= nwritten;
        ptr   += nwritten;
    }
    return(n);
}

void Writen(int fd, void *ptr, size_t nbytes)
{
    if (writen(fd, ptr, nbytes) != nbytes)
        err_sys("writen");
}

void Writen(int fd, std::string msg)
{
    if(fdIsValid(fd))
    {
        size_t len = msg.length();
        if (writen(fd, (void *) msg.c_str(), len) != len)
            err_sys("writen");
    }
}

void Bind(int fd, const struct sockaddr *sa, socklen_t salen)
{
    if (bind(fd, sa, salen) < 0)
        err_sys("bind");
}

void Listen(int fd, int backlog)
{
    char	*ptr;

    /*4can override 2nd argument with environment variable */
    if ( (ptr = getenv("LISTENQ")) != NULL)
        backlog = atoi(ptr);

    if (listen(fd, backlog) < 0)
        err_sys("listen");
}

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
           struct timeval *timeout)
{
    int		n;

    if ( (n = select(nfds, readfds, writefds, exceptfds, timeout)) < 0)
        err_sys("select");
    return(n);		/* can return 0 on timeout */
}

int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr)
{
    int		n;

again:
    if ( (n = accept(fd, sa, salenptr)) < 0)
    {
        if (errno == ECONNABORTED || errno == EAGAIN)
            goto again;
        else
            err_sys("accept");
    }
    return(n);
}

ssize_t Read(int fd, void *ptr, size_t nbytes)
{
    ssize_t		n;

    if ( (n = read(fd, ptr, nbytes)) == -1)
        err_sys("read");
    return(n);
}

const char * Inet_ntop(int family, const void *addrptr, char *strptr, size_t len)
{
    const char	*ptr;

    if (strptr == NULL)		/* check for old code */
        err_sys("NULL 3rd argument to inet_ntop");
    if ((ptr = inet_ntop(family, addrptr, strptr, (socklen_t) len)) == NULL)
    {
        cerr << strerror(errno)<< endl;
        err_sys("inet_ntop");		/* sets errno */
    }
    return(ptr);
}
