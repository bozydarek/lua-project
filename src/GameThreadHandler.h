#ifndef GAMETREADHANDLER_H
#define GAMETREADHANDLER_H

#include <mutex>
#include <condition_variable>
#include <string>
#include "LuaGameHandler.h"
#include "ThreadSafeQueue.h"

using namespace std;

class GameThreadHandler
{
private:
    string fullName;
    string shortName;
    string fileNamePath;
public:
    ThreadSafeQueue<tuple<int, int, string>> queue; //clientId, clientFD, message

public:
    GameThreadHandler(string _fullName, string _shortName, string _fileNamePath);

    void sendMessage(int _clientId, int _clientFD, string _message);

    const string &getFileNamePath() const;
    const string &getFullName() const;
    const string &getShortName() const;
};

#endif // GAMETREADHANDLER_H
