#include "Logger.h"

Logger::Logger(std::shared_ptr<ThreadSafeQueue<string> > _queue) : queue{_queue}
{

}

void Logger::write(string msg)
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&now_c), "%F %T");
    string logMsg = ss.str()  + ": " + msg;

    std::async(std::launch::async, [&]{ queue->push(logMsg); });
}
