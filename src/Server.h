#ifndef SERVER_H
#define SERVER_H

#include "ThreadFunctions.h"
#include "ConnectedClient.h"
#include "Logger.h"
#include "usersdatabase.h"
#include "posixwrapper.h"
#include "GameThreadHandler.h"
#include "LuaGameHandler.h"

#include <map>
#include <memory>
#include <vector>
#include <thread>

class Server
{
private:
    std::shared_ptr<UsersDatabase> usersDatabase;

    std::unique_ptr<Logger> logger;
    map<int, ConnectedClient> clients;
    map<string, shared_ptr<GameThreadHandler> > games;
    vector<thread> gameThreads;


    int					maxi, maxfd, listenfd, connfd, sockfd;
    int					nready, client[FD_SETSIZE];
    ssize_t				n;
    fd_set				rset, allset;
    char				buf[MAXLINE];
    socklen_t			clilen;
    struct sockaddr_in	cliaddr, servaddr;

    bool readableDescriptors = true;

    bool & serverShouldEnd;

    void checkForNewClientConnection();

    void checkAllClientsForData();

    void createGameThreadHandlers(vector<string> gamesPaths);


public:
    Server(std::shared_ptr<UsersDatabase> _usersDatabase,
           uint16_t serverPort,
           vector<string> gamesPaths,
           std::shared_ptr<ThreadSafeQueue<string>> _loggerQueue,
           bool & _serverShouldEnd);

    ~Server();
    void loop();
};

#endif // SERVER_H
