#ifndef POSIXWRAPPER_H
#define POSIXWRAPPER_H

#include <iostream>
#include <string>

#include <sys/types.h>	/* basic system data types */
#include <sys/socket.h>	/* basic socket definitions */

#include <netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>	/* inet(3) functions */
#include <unistd.h>

#include <errno.h>
#include <stdlib.h>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>

using namespace std;

#define	MAXLINE	4096	/* max text line length */
#define	SERV_PORT 9877			/* TCP and UDP */
#define	LISTENQ	1024	/* 2nd argument to listen() */
#define	SA	struct sockaddr

int fdIsValid(int fd);

int     Socket(int, int, int);
void    Bind(int, const SA *, socklen_t);
void    Listen(int, int);
int     Select(int, fd_set *, fd_set *, fd_set *, struct timeval *);
int     Accept(int, SA *, socklen_t *);
ssize_t Read(int, void *, size_t);
void    Close(int);
void    Writen(int, void *, size_t);
void    Writen(int fd, std::string msg);
void    err_sys(string funcName); /* error function */

const char * Inet_ntop(int family, const void *addrptr, char *strptr, size_t len);

#endif // POSIXWRAPPER_H
