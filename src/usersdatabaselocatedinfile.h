#ifndef USERSDATABASELOCATEDINFILE_H
#define USERSDATABASELOCATEDINFILE_H

using namespace std;

#include "usersdatabase.h"
#include <vector>
#include <fstream>

class UsersDatabaseLocatedInFile : public UsersDatabase
{    
public:
    UsersDatabaseLocatedInFile(string fileName);
    virtual ~UsersDatabaseLocatedInFile();


    bool userExists(string userName) override;
    bool passwordCorrect(string userName, string userPassword) override;
    int getUserId(string userName) override;
    UserType getUserType(string userName) override;

    std::string getUserName(int userID) override;
    UserType    getUserType(int userID) override;

    unsigned long getNumberOfUsers() override;
    std::vector<std::shared_ptr<User>> getConnectedUsers() override;
    void storeUserStatus(std::string userName, UserStatus newStatus) override ;
    UserStatus getUserStatus(std::string userName) override;
private:
    string fileName;

    vector<User> users;

    User& getUserByName(string userName);
    User& getUserByID(int userID);
};

#endif // USERSDATABASELOCATEDINFILE_H
