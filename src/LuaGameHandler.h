#ifndef LuaGameHandler_H
#define LuaGameHandler_H

#include <string>
#include <chrono>
#include "LuaState.h"

class LuaGameHandler {
    LuaState        state;
    std::string     name;
    std::string     shortName;
    int             minimumNumberOfPlayers;
    int             maximumNumberOfPlayers;
    std::chrono::time_point<std::chrono::system_clock> lastRoundStartTimePoint;
    std::vector<int> playersID;

    std::string     lastError = "";
public:

    LuaGameHandler();
    bool load(std::string pathToLuaFile);

    const std::string   &getFullName() const;
    const std::string   &getShortName() const;
    const int           &getMinimumNumberOfPlayers() const;
    const int           &getMaximumNumberOfPlayers() const;
    const std::string   &getLastLoadError() const;
    const std::chrono::time_point<std::chrono::system_clock> &getLastRoundStartTimePoint() const;



    const std::vector<int> getPlayersIDVector() const;
    void                prepareGame(std::vector<int> &vector);
    void                startGame();
    std::string         handlePlayerMove(int playerID, std::string move);
    void                playRound();
    std::map<int,int>   getScore();
    std::map<int,std::string> getNotifyList();
    bool                isGameOver();
    std::string         getView();
    std::string         handleAdminFunctions(std::string message);

private:
    static std::string testGame(std::string pathToLuaFile);
    bool loadGame(std::string pathToLuaFile);
};


#endif // LuaGameHandler_H
