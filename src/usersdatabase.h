#ifndef USERSDATABASE_H
#define USERSDATABASE_H

using namespace std;
#include <string>
#include <memory>
#include <vector>
#include "User.h"

class UsersDatabase
{
public:
    virtual bool        userExists(string userName) = 0;
    virtual bool        passwordCorrect(string userName, string userPassword) = 0;
    virtual int         getUserId(string userName) = 0;
    virtual UserType    getUserType(string userName) = 0;
    virtual unsigned long getNumberOfUsers() = 0;
    
    virtual std::string getUserName(int userID) = 0;
    virtual UserType    getUserType(int userID) = 0;

    virtual std::vector<std::shared_ptr<User>> getConnectedUsers() = 0;
    virtual void storeUserStatus(std::string userName, UserStatus newStatus) = 0;
    virtual UserStatus getUserStatus(std::string userName) = 0;
};

#endif // USERSDATABASE_H
