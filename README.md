## Project for Lua course ##

### Lua Game Server ###
Multithreaded, universal server to host games written in Lua.
Allows you to easily add new games without change in source code
of the server.

## Requirements ##

```
#!bash
apt install g++ cmake
apt install lua5.3 liblua5.3-0-dbg liblua5.3-dev

```

## Contributors ##
* Adam Sawicki
* Piotr Szymajda